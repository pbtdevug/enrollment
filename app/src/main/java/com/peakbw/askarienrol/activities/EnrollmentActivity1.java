package com.peakbw.askarienrol.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.peakbw.askarienrol.R;
import com.peakbw.askarienrol.db.ReaderContract;
import com.peakbw.askarienrol.db.ReaderContract.Employee;
import com.peakbw.askarienrol.db.ReaderDBHelper;
import com.peakbw.askarienrol.fragments.DatePickerFragment;

import java.util.ArrayList;

public class EnrollmentActivity1 extends Activity implements AdapterView.OnItemSelectedListener,View.OnFocusChangeListener{
	
	private String gender = "MALE",dateOfBirth,staffPin,appointmentDate;
	public static final String activityKey = "EnrollmentActivity1";
	private static EnrollmentActivity1 enrollActivity1;
	private String idScanned,photoCaptured,selectedSiteId,selectedBankId,nssf="0",account="0";
	public static EditText dateField,phoneField,appointmentField,pinField,nssfField,accField;
    public static  int appointmentView,dobView;
    private ArrayList<String> sitesList,banksList;
    private Spinner sitesSpinner,banksSpinner;
	
	private String employee,employeeId,employer,phone,employerId,idFile,photoFile;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_enrollment_activity1);
		// Show the Up button in the action bar.
		setupActionBar();
		
		setTitle("Staff Details");

		Log.d("EnrolmentActivity1", "URL = " + MainActivity.url);

		dateField = (EditText) findViewById(R.id.datePicker);
        dateField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if (id == R.id.next || id == EditorInfo.IME_NULL) {
                    validate();
                    return true;
                }
                return false;
            }
        });
        dateField.setOnFocusChangeListener(this);
		phoneField = (EditText) findViewById(R.id.phoneNumber);
        phoneField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if (id == R.id.next || id == EditorInfo.IME_NULL) {
                    validate();
                    return true;
                }
                return false;
            }
        });

		appointmentField = (EditText) findViewById(R.id.appointDate);
        appointmentField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if (id == R.id.next || id == EditorInfo.IME_NULL) {
                    validate();
                    return true;
                }
                return false;
            }
        });
        appointmentField.setOnFocusChangeListener(this);
		pinField = (EditText) findViewById(R.id.pin);
        pinField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if (id == R.id.next || id == EditorInfo.IME_NULL) {
                    validate();
                    return true;
                }
                return false;
            }
        });
        nssfField = (EditText) findViewById(R.id.nssf);
        nssfField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if (id == R.id.next || id == EditorInfo.IME_NULL) {
                    validate();
                    return true;
                }
                return false;
            }
        });
        accField = (EditText) findViewById(R.id.acc);
        accField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if (id == R.id.next || id == EditorInfo.IME_NULL) {
                    validate();
                    return true;
                }
                return false;
            }
        });
        sitesSpinner = (Spinner) findViewById(R.id.siteSpinner);
        banksSpinner = (Spinner) findViewById(R.id.bankSpinner);

        sitesList = new ArrayList<>();
        banksList = new ArrayList<>();
		
		Intent intent = getIntent();
		getDataFromPreviousActivity(intent);
		
		enrollActivity1 = this;

        dobView = R.id.datePicker;
        appointmentView = R.id.appointDate;
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {
		assert getActionBar() != null;
		getActionBar().setDisplayHomeAsUpEnabled(true);

	}
	
	@Override
    protected void onStart() {
        super.onStart();

		dateField.setText(dateOfBirth);
		phoneField.setText(phone);
		appointmentField.setText(appointmentDate);
		pinField.setText(staffPin);
        //nssfField.setText(nssf);
        //accField.setText(account);

        /*TextWatcher textWatcher = new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Integer inputLength = s.length();
                if(inputLength>0){
                    int limit = (int)((Integer.parseInt(s.toString()))*0.3);
                    limitField.setText(String.valueOf(limit));
                }

            }

            public void afterTextChanged(Editable s) {}
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        };

        salaryField.addTextChangedListener(textWatcher);*/
        String emp = employee + "-" + employeeId;
        TextView employeeView = (TextView) findViewById(R.id.employee);
        employeeView.setText(emp);

        ReaderDBHelper dbHelper = new ReaderDBHelper(getBaseContext());
        //run query that returns all sites
        Cursor cursor = dbHelper.getSites();
        int rows = cursor.getCount();
        Log.d(activityKey, "Site Rows = " + rows);

        while(cursor.moveToNext()){
            String siteName = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Sites.SITE_NAME));
            String siteID = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Sites.SITE_ID));

            /*SiteItem site = new SiteItem();
            site.setSiteId(siteID);
            site.setSiteName(siteName);*/
            String site = siteName+" - "+siteID;
            sitesList.add(site);
        }

        ArrayAdapter<String> sitesAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,sitesList);
        sitesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sitesSpinner.setAdapter(sitesAdapter);
        try{
            selectedSiteId = ((sitesSpinner.getSelectedItem().toString()).split("-"))[1];
        }catch(NullPointerException ex){
            ex.printStackTrace();
        }

        cursor = dbHelper.getBanks();

        rows = cursor.getCount();
        Log.d(activityKey, "Bank Rows = " + rows);

        String bank = "UNASSIGNED - 0";
        banksList.add(bank);

        while(cursor.moveToNext()){
            String bankName = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Banks.BANK_NAME));
            String bankID = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Banks.BANK_ID));

            /*SiteItem site = new SiteItem();
            site.setSiteId(siteID);
            site.setSiteName(siteName);*/
            bank = bankName+" - "+bankID;
            banksList.add(bank);
        }
        cursor.close();
        dbHelper.close();

        ArrayAdapter<String> bankAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,banksList);
        bankAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        banksSpinner.setAdapter(bankAdapter);

        selectedBankId = ((banksSpinner.getSelectedItem().toString()).split("-"))[1];
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
    	}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }
    @Override
    protected void onPause() {
        super.onPause();
      //time out the
        /*handler.postDelayed(new Runnable() {
        	@Override
        	public void run() {
        		finish();
      			}
        }, 120000 );*/
        // Another activity is taking focus (this activity is about to be "paused").
    }
    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
    }
    
	public void showDatePickerDialog(View v) {
        int id = v.getId();
        createDateFragment(id);
    }
    
    public void next(View view){
        validate();
    }

    private void validate(){
        appointmentDate = appointmentField.getText().toString();
        staffPin = pinField.getText().toString();
        phone = phoneField.getText().toString();
        dateOfBirth = dateField.getText().toString();
        nssf = nssfField.getText().toString();
        account = accField.getText().toString();

        Log.d(activityKey, "DOA = "+appointmentDate+"\nDOB = "+dateOfBirth+"\nNSSF = "+nssf+"\nAccount Number ="+account+"Phone ="+phone+"\nPIN = "+staffPin+"\n");

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(phone)){
            phoneField.setError(getString(R.string.error_phone_required));
            focusView = phoneField;
            cancel = true;
        }

        if (TextUtils.isEmpty(dateOfBirth)){
            dateField.setError(getString(R.string.error_dob_required));
            focusView = dateField;
            cancel = true;
        }

        if (dateOfBirth.length()!=10){
            dateField.setError(getString(R.string.error_date));
            focusView = dateField;
            cancel = true;
        }

        if(appointmentDate.length()!=10){
            appointmentField.setError(getString(R.string.error_date));
            focusView = appointmentField;
            cancel = true;
        }

        if(staffPin.length() > 0 && staffPin.length() != 4){
            dateField.setError(getString(R.string.error_pin_required));
            focusView = dateField;
            cancel = true;
        }

        if(cancel){
            focusView.requestFocus();
        }else {
            appointmentField.setText("");
            pinField.setText("");
            phoneField.setText("");
            dateField.setText("");
            nssfField.setText("");
            accField.setText("");

            ReaderDBHelper rDbHelper = new ReaderDBHelper(getBaseContext());
            Cursor cursor = rDbHelper.checkPhoneNumber(phone);
            assert cursor != null;
            //check if phone number was not used by another person
            if(cursor.moveToFirst()){
                String name = cursor.getString(cursor.getColumnIndexOrThrow(Employee.EMPLOYEE_NAME));
                String id = cursor.getString(cursor.getColumnIndexOrThrow(Employee.EMPLOYEE_ID));

                createAlertDialog(phone+" was used by "+name+"-"+id+". \nDo you want to do an update?",R.layout.confirmation_prompt,R.id.message);
            }
            else{

                startEnrollmentActivity2();
            }
            cursor.close();
            rDbHelper.close();
        }
    }
    
    public void cancel(View view){
    	finish();
    }
    
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.male:
                if (checked)
                    gender = "MALE";
                break;
            case R.id.female:
                if (checked)
                	gender = "FEMALE";
                break;
        }
    }
    
    private void startEnrollmentActivity2(){
    	Intent enrollment2Intent = new Intent(this,EnrollmentActivity2.class);
        String EnrollmentData = ceateEnrollmentData();
        Log.d(activityKey,"Enrollment Data"+EnrollmentData);
    	enrollment2Intent.putExtra(EnrollmentActivity1.activityKey,ceateEnrollmentData());
    	
    	enrollment2Intent.putExtra(EmployerActivity.ID_FILE,idFile);

    	enrollment2Intent.putExtra(EmployerActivity.PHOTO_FILE,photoFile);
    	enrollment2Intent.putExtra(EmployerActivity.ID_SCANNED,idScanned);
    	enrollment2Intent.putExtra(EmployerActivity.PHOTO_CAPTURED,photoCaptured);
    	
    	startActivity(enrollment2Intent);
    }
    
    public String ceateEnrollmentData(){
        selectedBankId = ((banksSpinner.getSelectedItem().toString()).split("-"))[1];
        selectedBankId = selectedBankId.trim();
        Log.d(activityKey,"Selected Bank ID :"+selectedBankId);
        selectedSiteId = ((sitesSpinner.getSelectedItem().toString()).split("-"))[1];
        selectedSiteId = selectedSiteId.trim();
        if(staffPin.length() == 0){
            staffPin = "0";
        }
    	return employer+"*"+employee+"*"+dateOfBirth+"*"+gender+"*"+phone+"*"+appointmentDate+"*"+staffPin+"*"+employerId+"*"+employeeId+"*"+selectedSiteId+"*"+selectedBankId+"*"+nssf+"*"+account;
    }
    
    public static EnrollmentActivity1 getInstance(){
		return enrollActivity1;
	}
    
    private void getDataFromPreviousActivity(Intent intent){
    	if(intent.hasExtra(EmployerActivity.EMPLOYEE)){
    		employee = intent.getStringExtra(EmployerActivity.EMPLOYEE); 
    		employeeId = intent.getStringExtra(EmployerActivity.EMPLOYEE_ID);
    		employer = intent.getStringExtra(EmployerActivity.EMPLOYER);
    		dateOfBirth = intent.getStringExtra(EmployerActivity.DOB);
    		staffPin = intent.getStringExtra(EmployerActivity.STAFF_PIN);
    		appointmentDate = intent.getStringExtra(EmployerActivity.APPOINTMENT_DATE);
    		phone = intent.getStringExtra(EmployerActivity.PHONE);
    		employerId = intent.getStringExtra(EmployerActivity.EMPLOYER_ID);
    	
    		idFile = intent.getStringExtra(EmployerActivity.ID_FILE);
    		photoFile = intent.getStringExtra(EmployerActivity.PHOTO_FILE);
    	
    		idScanned = intent.getStringExtra(EmployerActivity.ID_SCANNED);
    		photoCaptured = intent.getStringExtra(EmployerActivity.PHOTO_CAPTURED);
    	}
    }
    
    private void createAlertDialog(String msg,int layout,int textView){
    	LayoutInflater li = LayoutInflater.from(this);
		View promptsView = li.inflate(layout, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setView(promptsView);
		
		final TextView alertTextView = (TextView) promptsView.findViewById(textView);
		alertTextView.setText(msg);
		
		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog,int id) {
			    	startEnrollmentActivity2();
			    }
			  }).setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
				  public void onClick(DialogInterface dialog,int id) {
					  dialog.cancel();
				  }
		});
		
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        int viewId = adapterView.getId();
        Log.d(activityKey, "Selected View id = " + viewId);
        if(R.id.siteSpinner == id){
            selectedSiteId = (((String)adapterView.getItemAtPosition(position)).split("-"))[1];
            Log.d(activityKey, "Selected Site = " + selectedSiteId);
        }else if(R.id.bankSpinner == id){
            selectedBankId = (((String)adapterView.getItemAtPosition(position)).split("-"))[1];
            Log.d(activityKey, "Selected Bank = " + selectedBankId);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if(hasFocus){
            int viewId = view.getId();
            if(viewId == R.id.datePicker || viewId == R.id.appointDate){
                createDateFragment(view.getId());
            }
        }
    }

    private void createDateFragment(int viewId){
        DialogFragment dateFragment = new DatePickerFragment();
        Bundle args = new Bundle();
        args.putInt("view", viewId);
        dateFragment.setArguments(args);

        dateFragment.show(getFragmentManager(), "Calender");
    }
}
