package com.peakbw.askarienrol.activities;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.peakbw.askarienrol.R;
import com.peakbw.askarienrol.db.ReaderContract;
import com.peakbw.askarienrol.db.ReaderContract.State;
import com.peakbw.askarienrol.db.ReaderDBHelper;
import com.peakbw.askarienrol.net.ManageServerConnections;
import com.peakbw.askarienrol.net.XMLParser;
import com.peakbw.askarienrol.util.SyncTask;

public class ConfirmationActivity extends Activity {
	
	private static final String DEBUG_TAG = "DownloadXMLTask";
	public static final String activityKey = "ConfirmationActivity";
    private String id,photo,employer,employee,gender,dob;
	private static String phone;
	private  ManageServerConnections mdc;
	private String appointmentDate;
	private String staffPin;
	private String employeeId;
	private String employerId;
	private String idScanned;
	public static ProgressDialog progressDialog;
    public static final int DIALOG_REQUEST_PROGRESS = 0;
	public static final int DIALOG_VERIFICATION = 1;
	private String photoCaptured,nssf,account,bankId,siteId,stateId;
	private String veriXML;
    private Context activityContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_confirmation);
		// Show the Up button in the action bar.

		setupActionBar();
		setTitle("Confirm Details");
		mdc = new ManageServerConnections();
        activityContext = this;
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {
        assert getActionBar() !=null;
		getActionBar().setDisplayHomeAsUpEnabled(true);

	}
	
	@SuppressLint("DefaultLocale")
	@Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.

        Log.d(DEBUG_TAG,"URL = "+MainActivity.url);

        Intent intent = getIntent();
        
        if(intent.hasExtra(EnrollmentActivity2.activityKey)){
            String employeeDAta = intent.getStringExtra(EnrollmentActivity2.activityKey);
        	setEmployeeFields(employeeDAta);
        }
        
        TextView employerv = (TextView) findViewById(R.id.employer);
        TextView employeev = (TextView) findViewById(R.id.cemployee);
        TextView dobv = (TextView) findViewById(R.id.dobirth);
        TextView genderv = (TextView) findViewById(R.id.gender);
        TextView idScannedv = (TextView) findViewById(R.id.cid);
        TextView photoCapturedv = (TextView) findViewById(R.id.photo);
        TextView appDateView = (TextView) findViewById(R.id.appointDate);
        TextView staffPinView = (TextView) findViewById(R.id.staffPIN);
        TextView phonev = (TextView) findViewById(R.id.phone);

        TextView nssfView = (TextView) findViewById(R.id.nssfView);
        TextView accView = (TextView) findViewById(R.id.accfView);
        TextView bankView = (TextView) findViewById(R.id.bankView);
        TextView siteView = (TextView) findViewById(R.id.siteView);
        
        employerv.setText(employer.toUpperCase());
        employeev.setText(employee);
        dobv.setText(dob);
        genderv.setText(gender);
        phonev.setText(phone);

        idScannedv.setText(idScanned.toUpperCase());
        photoCapturedv.setText(photoCaptured.toUpperCase());
        appDateView.setText(appointmentDate);
        staffPinView.setText(staffPin);

        nssfView.setText(nssf);
        accView.setText(account);

		ReaderDBHelper dbHelper = null;
		try{
			dbHelper = new ReaderDBHelper(getBaseContext());
			Cursor cursor = dbHelper.getBank(bankId.trim());
			if(!cursor.isClosed()){
				int count = cursor.getCount();
				Log.d(DEBUG_TAG,"Bank Rows : "+count);
				if(count>0){
					cursor.moveToFirst();
					String BankName = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Banks.BANK_NAME));
					bankView.setText(BankName);
				}

				cursor = dbHelper.getSite(siteId.trim());
				if(!cursor.isClosed()){
					count = cursor.getCount();
					Log.d(DEBUG_TAG,"Site Rows : "+count);
					if(count>0){
						cursor.moveToFirst();
						String siteName = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Sites.SITE_NAME));
						siteView.setText(siteName);
					}
					cursor.close();
				}

				cursor.close();

			}

		}catch (Exception ex){
			ex.printStackTrace();
		}finally {
			if(dbHelper != null){
				dbHelper.close();
			}
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case android.R.id.home:
			Log.d(DEBUG_TAG, "Up button clicked!");
			finish();
			return true;
    	}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }
    @Override
    protected void onPause() {
        super.onPause();
      //time out the
        /*handler.postDelayed(new Runnable() {
        	@Override
        	public void run() {
        		finish();
      			}
        }, 120000 );*/
        // Another activity is taking focus (this activity is about to be "paused").
    }
    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
    }
    
    public void confirmationStateMachine(View view){
    	if(view.getId()==R.id.ccancel){
    		EnrollmentActivity2.getInstance().finish();
    		EnrollmentActivity1.getInstance().finish();
    		finish();
    	}
    	else{
            if(idScanned.equals("true")){
                if(photoCaptured.equals("true")){
                    updateStateTable(EnrollmentActivity2.STATE_ENROLLMENT,"",this);
                    //start ftp service
                    new SyncTask(this).execute(getString(R.string.ftpServerHost),getString(R.string.ftpServerUsername),getString(R.string.ftpServerPassword),getString(R.string.ftpServerPort));

                    String xmlRequest = createEnrollmentXML();
                    mdc.setUpdateVariable(xmlRequest);

					ReaderDBHelper dbHelper = new ReaderDBHelper(getBaseContext());
					// get connection url
					Cursor cursor = dbHelper.getSignature();
					cursor.moveToFirst();
					String url = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Signature.UPDATE_URL));
					Log.d(DEBUG_TAG, "URL = "+url);

					dbHelper.close();
                    new DownloadXMLTask().execute(url);
                }
                else{
                    createAlertDialog("Photograph not captured.\nDo you want to save Enrollment as pending!",R.layout.confirmation_prompt,R.id.message);
                }
            }
            else{
                createAlertDialog("ID not Scanned.\nDo you want to save Enrollment as pending!",R.layout.confirmation_prompt,R.id.message);
            }
    	}
    	
    }
    
    public class DownloadXMLTask extends AsyncTask<String, Void, String> {
		private String status;
		private InputStream stream = null;
		private static final String DEBUG_TAG = "DownloadXMLTask";

		@SuppressWarnings("deprecation")
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(DIALOG_REQUEST_PROGRESS);
		}
		
		@Override
		protected String doInBackground(String... url) {
			try {
	    		stream = mdc.downloadUrl(url[0]);
	    		try	{
	    			status = XMLParser.parseXML(stream,getBaseContext());
	    		}
	    		catch(Exception ex){
	    			ex.printStackTrace();
	    		}
	    	}
	    	catch(IOException ex){
	        	ex.printStackTrace();
	        }
	        finally {
	            if (stream != null) {
	            	try{
	            		stream.close();
	                }catch(IOException ex){
	                	ex.printStackTrace();
	                }
	            }
	        }
	    	return status;
		}

		@SuppressWarnings("deprecation")
		@Override
	    protected void onPostExecute(String result) {
			dismissDialog(DIALOG_REQUEST_PROGRESS);

            Log.d(DEBUG_TAG, "Enrollment Status = "+EmployerActivity.newStaff);
            if(EmployerActivity.newStaff){
                insertEmployee(employee,employeeId,employerId,stateId,activityContext);
            }

	    	if(result!=null){
	    		Log.d(DEBUG_TAG, "Results = "+result);
	    		if(result.contains("SUCCESSFUL")&&XMLParser.method.equals("askari_enrollment")){
	    			//updateStateTable(EnrollmentActivity2.STATE_VERIFICATION);
                    updateStateTable(EnrollmentActivity2.STATE_COMPLETE,stateId,getBaseContext());
	    			//startVerification();
                    createResponseDialog(result,R.layout.success_prompt,R.id.success);
	    		}
	    		else if(result.contains("SUCCESSFUL")&&XMLParser.method.equals("askari_verification")){
	    			updateStateTable(EnrollmentActivity2.STATE_COMPLETE,"",getBaseContext());
	    			createResponseDialog(result,R.layout.success_prompt,R.id.success);
	    		}
	    		else{
	    			if(ManageServerConnections.timeout==null){
                            updateStateTable(EnrollmentActivity2.STATE_VERIFICATION,"",getBaseContext());
	    				    createResponseDialog(result,R.layout.error_prompt,R.id.error);
	    			}
	    			else{
                        //deleteRow(phone);
	    				createResponseDialog(ManageServerConnections.timeout,R.layout.error_prompt,R.id.error);
	    			}
	    		}
	    	}
	    	else{
                createResponseDialog("No internet connection", R.layout.error_prompt, R.id.error);
	    	}

	    	//clear fields
			try {
				EnrollmentActivity1.dateField.setText("");
				EnrollmentActivity1.phoneField.setText("");
				EnrollmentActivity1.appointmentField.setText("");
				EnrollmentActivity1.pinField.setText("");
	    	}catch (NullPointerException ex){
	    		ex.printStackTrace();
			}
	    }
	}
    
    private void createResponseDialog(String msg,int layout,int textView){
    	LayoutInflater li = LayoutInflater.from(this);
		View promptsView = li.inflate(layout, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setView(promptsView);
		
		final TextView successTextView = (TextView) promptsView.findViewById(textView);
		successTextView.setText(msg);
		
		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog,int id) {
			    	//finish previous activities
					try {
						EnrollmentActivity2.getInstance().finish();
						EnrollmentActivity1.getInstance().finish();
						EmployerActivity.getInstance().finish();
						//EmployerActivity.newStaff = false;
						startEmployerActivity();
					}catch (Exception ex){
						ex.printStackTrace();
					}
			    	finish();
			    }
			  });
		
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
    }
    
    public String createEnrollmentXML(){
    	StringBuilder xmlStr = new StringBuilder();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<pbtRequest>");
        xmlStr.append("<method>").append("askari_enrollment").append("</method>");

		ReaderDBHelper dbHelper = new ReaderDBHelper(getBaseContext());

		Cursor cursor = dbHelper.getUserCredentials();
		cursor.moveToFirst();
		String password = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Auth.USER_PIN));
		String username = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Auth.USER_NAME));

		xmlStr.append("<password>").append(password).append("</password>");
		xmlStr.append("<username>").append(username).append("</username>");

		cursor = dbHelper.getSignature();
		cursor.moveToFirst();
		String signature = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Signature.SIGNATURE));
		cursor.close();
		dbHelper.close();

		xmlStr.append("<signature>").append(signature).append("</signature>");
        //xmlStr.append("<imei>").append(MainActivity.phoneID.trim()).append("</imei>");
        //xmlStr.append("<iccid>").append(MainActivity.simSerialNumber).append("</iccid>");
        xmlStr.append("<trxnNumber>").append(ManageServerConnections.getTrxnNumber()).append("</trxnNumber>");
        xmlStr.append("<employerName>").append(employer).append("</employerName>");
        xmlStr.append("<employerID>").append(employerId.trim()).append("</employerID>");
        xmlStr.append("<employeeName>").append(employee).append("</employeeName>");
        xmlStr.append("<employeeID>").append(employeeId.trim()).append("</employeeID>");
		xmlStr.append("<siteId>").append(siteId.trim()).append("</siteId>");
		xmlStr.append("<bankId>").append(bankId.trim()).append("</bankId>");
		xmlStr.append("<nssf>").append(nssf).append("</nssf>");
		xmlStr.append("<accNum>").append(account).append("</accNum>");
        String phoneNumber = phone.replaceFirst("0","256");
        xmlStr.append("<phoneNumber>").append(phoneNumber).append("</phoneNumber>");
        xmlStr.append("<gender>").append(gender).append("</gender>");
        xmlStr.append("<dateOfBirth>").append(dob).append("</dateOfBirth>");
        xmlStr.append("<idScan>").append(id).append("</idScan>");
        xmlStr.append("<photo>").append(photo).append("</photo>");
        xmlStr.append("<appointmentDate>").append(appointmentDate).append("</appointmentDate>");
        xmlStr.append("<employeePin>").append(staffPin.trim()).append("</employeePin>");
        xmlStr.append("</pbtRequest>");
        
        MainActivity.activeEmployee = employee+"-"+employeeId;
        
        System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }
    
    /*private String createVerificationXML(){
    	StringBuilder xmlStr = new StringBuilder();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<pbtRequest>");
        xmlStr.append("<method>").append("askari_verification").append("</method>");
        xmlStr.append("<pin>").append(MainActivity.pin).append("</pin>");
        xmlStr.append("<imei>").append(MainActivity.phoneID).append("</imei>");
        xmlStr.append("<iccid>").append(MainActivity.simSerialNumber).append("</iccid>");
        xmlStr.append("<employeeID>").append(employeeId).append("</employeeID>");
        xmlStr.append("<employerID>").append(employerId).append("</employerID>");
        System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }*/
    
    public static int updateStateTable(String state,String id,Context context){
		ReaderDBHelper dbHelper = new ReaderDBHelper(context);
    	SQLiteDatabase db = dbHelper.getReadableDatabase();

    	// New value for one column
    	ContentValues values = new ContentValues();
    	values.put(State.STATUS, state);

    	// Which row to update, based on the ID
        int count = 0;
        //if(!employeeId.equals("")){
			//values.put(State.EMPLOYEE_ID, employeeId);
            String selection = State._ID + " = ?";
            String[] selectionArgs = {id};

            count = db.update(State.TABLE_NAME,values,selection,selectionArgs);

            values.clear();
            Log.d(DEBUG_TAG, "Affected Row ID = "+count);

            db.close();
            dbHelper.close();
        //}

    	return count;
    }

    public static void insertEmployee(String name,String id,String employerID,String stateId,Context context){
		ReaderDBHelper dbHelper = new ReaderDBHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ContentValues values = new ContentValues();

        //values.put(ReaderContract.Employee.EMPLOYEE_ID, id);
        values.put(ReaderContract.Employee.EMPLOYEE_NAME, name);
        values.put(ReaderContract.Employee.EMPLOYER_ID, employerID);
		values.put(ReaderContract.Employee.STATE_ID, stateId);

        Log.d(DEBUG_TAG, "State ID: "+ stateId);

        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insertWithOnConflict(ReaderContract.Employee.TABLE_NAME, ReaderContract.Employee.COLUMN_NAME_NULLABLE,values,SQLiteDatabase.CONFLICT_IGNORE);
        Log.d(DEBUG_TAG, "New Employee :"+ newRowId+" "+id);
        Log.d(DEBUG_TAG, "New Employee: "+ name);

		db.close();
		dbHelper.close();
    }
    
    private void setEmployeeFields(String info){
		ArrayList<String> emppDetails = ManageServerConnections.split(info, "*");
		this.employee = emppDetails.get(1);
		this.employer = emppDetails.get(0);
		this.employerId = emppDetails.get(7);
		this.employeeId = emppDetails.get(8);
		this.dob = emppDetails.get(2);
		this.gender = emppDetails.get(3);
		ConfirmationActivity.phone = emppDetails.get(4);
		this.staffPin = emppDetails.get(6);
		this.appointmentDate = emppDetails.get(5);

		this.siteId = emppDetails.get(9);
		this.bankId = emppDetails.get(10);
		this.nssf = emppDetails.get(11);
		this.account = emppDetails.get(12);

		this.idScanned = emppDetails.get(13);
		this.photoCaptured = emppDetails.get(14);
		this.id = emppDetails.get(15);
		this.photo = emppDetails.get(16);
		this.stateId = emppDetails.get(17);
	}
    
    private void createAlertDialog(String msg,int layout,int textView){
    	LayoutInflater li = LayoutInflater.from(this);
		View promptsView = li.inflate(layout, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setView(promptsView);
		
		final TextView alertTextView = (TextView) promptsView.findViewById(textView);
		alertTextView.setText(msg);
		
		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog,int id) {
                    updateStateTable(EnrollmentActivity2.STATE_DOCUMENTS,"",getBaseContext());
			    	EnrollmentActivity2.getInstance().finish();
			    	EnrollmentActivity1.getInstance().finish();
			    	finish();
			    }
			  }).setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
				  public void onClick(DialogInterface dialog,int id) {
					  dialog.cancel();
				  }
		});
		
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
    }
    
    @SuppressWarnings("deprecation")
	/*private void startVerification(){
    	veriXML = createVerificationXML();
    	showDialog(DIALOG_VERIFICATION);
    	//Intent veriIntent = new Intent(this,VerificationActivity.class);
		//veriIntent.putExtra(activityKey, createVerificationXML());
		//finish previous activities
		//EnrollmentActivity2.getInstance().finish();
		//EnrollmentActivity1.getInstance().finish();
		//
		//startActivity(veriIntent);
		//finish();
    }*/
    
    /*private void deleteRow(String phoneNumber){
		ReaderDBHelper dbHelper = new ReaderDBHelper(this);
    	SQLiteDatabase db = dbHelper.getReadableDatabase();
    	// Define 'where' part of query.
    	String selection = State.PHONE_NUMBER + " = ?";
    	// Specify arguments in placeholder order.
    	String[] selectionArgs = { phoneNumber };
    	// Issue SQL statement.
    	db.delete(State.TABLE_NAME, selection, selectionArgs);

		db.close();
		dbHelper.close();
    }*/
    
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
			case DIALOG_REQUEST_PROGRESS:
				progressDialog = new ProgressDialog(this);
				progressDialog.setMessage("please wait");
				progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progressDialog.setCancelable(true);
				progressDialog.show();
				
				return progressDialog;
				
			case DIALOG_VERIFICATION:
                AlertDialog.Builder vericationDialog = new AlertDialog.Builder(this);
				LayoutInflater li = LayoutInflater.from(this);
				View promptsView = li.inflate(R.layout.verification_prompt, null);
				final EditText veriInput = (EditText) promptsView.findViewById(R.id.verification_code);
				vericationDialog.setView(promptsView);
				vericationDialog.setCancelable(false);
				vericationDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					// do something when the button is clicked
					public void onClick(DialogInterface dialog, int id) {
			            String veriCode = veriInput.getText().toString();
			            if(veriCode.length()==6){
			    			veriXML = veriXML+"<code>"+veriCode+"</code>";
			    			veriXML = veriXML+"</pbtRequest>";
			    			mdc.setUpdateVariable(veriXML);
							// get connection url
							ReaderDBHelper dbHelper = new ReaderDBHelper(getBaseContext());
							Cursor cursor = dbHelper.getSignature();
							cursor.moveToFirst();
							String url = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Signature.UPDATE_URL));
							Log.d(DEBUG_TAG, "URL = "+url);
			    			new DownloadXMLTask().execute(url);
			    		}
			    		else{
			    			Toast.makeText(ConfirmationActivity.this,"Code must be 6 digits!", Toast.LENGTH_LONG).show();
			    		}
					}
				});
				vericationDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
					}
				});
				return vericationDialog.create();
				
			default:
				return null;
        }
    }

    private void startEmployerActivity(){
        Intent intent = new Intent(getBaseContext(),EmployerActivity.class);
        startActivity(intent);
    }
}
