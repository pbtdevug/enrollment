package com.peakbw.askarienrol.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.peakbw.askarienrol.R;
import com.peakbw.askarienrol.camera.AlbumStorageDirFactory;
import com.peakbw.askarienrol.camera.BaseAlbumDirFactory;
import com.peakbw.askarienrol.camera.FroyoAlbumDirFactory;
import com.peakbw.askarienrol.db.ReaderContract;
import com.peakbw.askarienrol.db.ReaderContract.State;
import com.peakbw.askarienrol.db.ReaderDBHelper;
import com.peakbw.askarienrol.net.ManageServerConnections;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Target;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static java.security.AccessController.getContext;

public class EnrollmentActivity2 extends Activity{
	
	private static final int ACTION_TAKE_PHOTO = 1;
	private static final int ACTION_SCAN_ID = 2;

    private static final int ID_REQUEST_CODE = 404;
    private static final int PHOTO_REQUEST_CODE = 405;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTNL_STORE = 100;

	private String employeeInfo;
	private static EnrollmentActivity2 enrollActivity;
	
	public static final String STATE_DOCUMENTS = "pending_documents";
	public static final String STATE_ENROLLMENT = "pending_enrollment";
	public static final String STATE_VERIFICATION = "pending_verification";
	public static final String STATE_COMPLETE = "complete";
	
	private static final String ID_BITMAP_STORAGE_KEY = "idbitmap";
    private static final String PHOTO_BITMAP_STORAGE_KEY = "photobitmap";

	private static final String ID_VISIBILITY_STORAGE_KEY = "idvisibility";
    private static final String PHOTO_VISIBILITY_STORAGE_KEY = "photovisibility";

    private static final String PHOTO_STORAGE_KEY = "photo";
    private static final String ID_STORAGE_KEY = "id";

	private Bitmap idBitmap,photoBitmap, bitmap;

	private int build_version = Build.VERSION.SDK_INT;


	private static String filePrefix;
	//private Button idPreviewBtn;
	private ImageView imageView;
	private ImageButton idImageView,photoImageView;
	private String mCurrentPhotoPath;
	public static boolean idScanned,photoCaptured;
	private String photo="",id="",gender,dob,phone,limit,salary,employer,employeeId,employerId,siteId,bankId,nssf,account;
	
	private static final String DEBUG_TAG = "EnrollmentActivity2";
	public static final String activityKey = "com.peakbw.loanapp.activities.EnrollmentActivity2";
	
	//private static final String JPEG_FILE_PREFIX = EmployerActivity.employeeId;
	private static final String JPEG_FILE_SUFFIX = ".jpg";
	private String photoName,stateId = "0";
	private AlbumStorageDirFactory mAlbumStorageDirFactory = null;
	public static String state;
	private  ContentValues values;
	private String photoPath = "", idPath = "";

    private int picker_icon;
    private InputStream is = null;
	private File file = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_enrollment_activity2);
		// Show the Up button in the action bar.
		setupActionBar();
		
		setTitle("Staff Photo");
		
		idImageView = findViewById(R.id.id_scan);
		photoImageView = findViewById(R.id.photo_scan);

        System.out.println("onC Target Width = "+idImageView.getWidth());
        System.out.println("onC Target Height = "+idImageView.getHeight());
		
		Intent intent = getIntent();

        picker_icon = R.drawable.picker;

        setEmployeeFields(intent);
        setIMageViews(intent);
        
        values = new ContentValues();
        
        enrollActivity = this;
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		    case android.R.id.home:
			    Log.d(DEBUG_TAG, "Up button clicked!");
			    finish();
			    return true;

            case R.id.action_take_photo:
                imageView = photoImageView;
                photo = dispatchTakePictureIntent(ACTION_TAKE_PHOTO);
                Log.d(DEBUG_TAG, "photoFile = "+photo);
                return true;

            case R.id.action_scan_id:
                imageView = idImageView;
                id = dispatchTakePictureIntent(ACTION_SCAN_ID);
                Log.d(DEBUG_TAG, "idScanFile = "+id);
                return true;
    	}


		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.

        Log.d(DEBUG_TAG,"URL = "+MainActivity.url);

        filePrefix = employerId+"_"+employeeId+"_";
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
			mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
		} else {
			mAlbumStorageDirFactory = new BaseAlbumDirFactory();
		}
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.enrollment_activity2, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }
    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
    }

    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d("Result Retuned", "Ok Clicked = ");
		if (resultCode == RESULT_OK&&requestCode != ID_REQUEST_CODE&&requestCode !=PHOTO_REQUEST_CODE) {
			handleBigCameraPhoto();
		}
        else if (resultCode == RESULT_OK && requestCode == ID_REQUEST_CODE){
            Uri uri = data.getData();
            String path = getPath(uri);
			Log.d("Image URI", "photo = "+path);
            id = extractName(path);
			if(id != ""){
            	setPic(path);
				InsertPic(path, 1);
				idPath = path;
			}

            super.onActivityResult(requestCode, resultCode, data);
        }
        else if (resultCode == RESULT_OK && requestCode == PHOTO_REQUEST_CODE){
            Uri uri = data.getData();
            String path = getPath(uri);
			Log.d("Image URI", "photo = "+path);
            photo = extractName(path);
			if(photo != ""){
            	setPic(path);
				InsertPic(path,2);
				photoPath = path;
			}

            super.onActivityResult(requestCode, resultCode, data);
        }
	}
    
    @Override
    // save activity state
	protected void onSaveInstanceState(Bundle outState) {

		outState.putParcelable(ID_BITMAP_STORAGE_KEY, idBitmap);
		outState.putBoolean(ID_VISIBILITY_STORAGE_KEY, (idBitmap != null) );

        outState.putParcelable(PHOTO_BITMAP_STORAGE_KEY, photoBitmap);
        outState.putBoolean(PHOTO_VISIBILITY_STORAGE_KEY, (photoBitmap != null) );

        outState.putString(ID_STORAGE_KEY,id);
        outState.putString(PHOTO_STORAGE_KEY,photo);

		super.onSaveInstanceState(outState);
	}
    
    @Override
    //restore activity state
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		idBitmap = savedInstanceState.getParcelable(ID_BITMAP_STORAGE_KEY);
        photoBitmap = savedInstanceState.getParcelable(PHOTO_BITMAP_STORAGE_KEY);
        //
        idImageView.setImageBitmap(idBitmap);
        idImageView.setVisibility(
                savedInstanceState.getBoolean(ID_VISIBILITY_STORAGE_KEY) ?
                        ImageView.VISIBLE : ImageView.INVISIBLE
        );

        //
        photoImageView.setImageBitmap(photoBitmap);
        photoImageView.setVisibility(
                savedInstanceState.getBoolean(PHOTO_VISIBILITY_STORAGE_KEY) ?
                        ImageView.VISIBLE : ImageView.INVISIBLE
        );


        id = savedInstanceState.getString(ID_STORAGE_KEY);
        photo = savedInstanceState.getString(PHOTO_STORAGE_KEY);
	}
    
    private void handleBigCameraPhoto() {
		if (mCurrentPhotoPath != null) {
			Log.d(DEBUG_TAG, "ImagePath = "+mCurrentPhotoPath);
			galleryAddPic();
			if(imageView == idImageView){
				idBitmap = setPic(mCurrentPhotoPath);
				InsertPic(mCurrentPhotoPath, 1);
				idPath = mCurrentPhotoPath;
			}else if(imageView == photoImageView){
				photoBitmap = setPic(mCurrentPhotoPath);
				InsertPic(mCurrentPhotoPath,2);
				photoPath = mCurrentPhotoPath;
			}
			mCurrentPhotoPath = null;
		}
	}
    
    public void enrollment2StateMachine(View view){
    	if(isIntentAvailable(this, MediaStore.ACTION_IMAGE_CAPTURE)){

            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);

    		if(view.getId()==R.id.idLabel){
    			imageView = idImageView;
				if(idPath.length() == 0){
                	startActivityForResult(intent, ID_REQUEST_CODE);
				}else{
					//rotatePic(idPath);
				}
    		}
    		else if(view.getId()==R.id.photoLabel){
    			imageView = photoImageView;
				if(photoPath.length() == 0){
                	startActivityForResult(intent, PHOTO_REQUEST_CODE);
				}else{
					//rotatePic(photoPath);
				}
    		}
    		else if(view.getId()==R.id.cancel){
        		EnrollmentActivity1.getInstance().finish();
    			finish();
    		}
    		else if(view.getId()==R.id.next){
    			getFormData();
    		}
    	}
    	else{
			//btn.setClickable(false);
    	}
    }
    
    /* Photo album for this application */
	private String getAlbumName() {
		return getString(R.string.album_name);
	}
	
	private String dispatchTakePictureIntent(int actionCode) {
		String fileName ="";
		switch(actionCode) {
		case ACTION_TAKE_PHOTO:
			photoName = filePrefix+"photo_";
			fileName = startCameraActivity(actionCode);
			Log.d("PictureIntent", "photo = "+fileName);
			break;
			
		case ACTION_SCAN_ID:
			photoName = filePrefix+"id_";
			fileName = startCameraActivity(actionCode);
			Log.d("PictureIntent", "id = "+fileName);
			break;
			
		default:
			break;			
		}
		return fileName;
	}
	
	private File setUpPhotoFile() throws IOException {
		
		File f = createImageFile();
		String file = f.getName();
		Log.d(DEBUG_TAG, "RealFileName = "+file);
		mCurrentPhotoPath = f.getAbsolutePath();
		
		return f;
	}
	
	private File createImageFile() throws IOException {
		// Create an image file name
		//String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = photoName;
		Log.d(DEBUG_TAG, "ImageName = "+imageFileName);
		File imageF = null;
		if(build_version >= 23){
			File albumF = new File(getFilesDir(), "Pictures");
			imageF = new File(albumF, imageFileName+JPEG_FILE_SUFFIX);
		}else{
			File albumF = getAlbumDir();
			imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
		}

		return imageF;
	}
	
	private File getAlbumDir() {
		File storageDir = null;
		try{
		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
			
			storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());

			if (storageDir != null) {
				if (! storageDir.mkdirs()) {
					if (! storageDir.exists()){
						Log.d("CameraSample", "failed to create directory");
						return null;
					}
				}
			}
			
		} 
		else {
			Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
		}
		}catch(Exception ex){
		}
		
		return storageDir;
	}
	
	public static boolean isIntentAvailable(Context context, String action) {
		final PackageManager packageManager = context.getPackageManager();
		final Intent intent = new Intent(action);

		List<ResolveInfo> list = packageManager.queryIntentActivities(intent,PackageManager.MATCH_DEFAULT_ONLY);

        return list.size() > 0;
	}

	private void galleryAddPic() {
		Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
		File f = new File(mCurrentPhotoPath);
		Uri contentUri = Uri.fromFile(f);
		mediaScanIntent.setData(contentUri);
		this.sendBroadcast(mediaScanIntent);

        //resize picture
        //resizePicture(contentUri);

	}
	@TargetApi(23)
	private Bitmap setPic(String photoPath) {

		/* Get the size of the ImageView */
		int targetW = 300;
		int targetH = 300;
		try{
			file = new File(photoPath);
			if(build_version > 21){
				if (ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
					ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},MY_PERMISSIONS_REQUEST_READ_EXTNL_STORE);
				}else{
					is = getContentResolver().openInputStream(Uri.fromFile(file));
				}
			}else{
				is = getContentResolver().openInputStream(Uri.fromFile(file));
			}
		}catch (Exception ex){
			ex.printStackTrace();
			return null;
		}

		if(is == null){
			return null;
		}

		/* Get the size of the image */
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = false;
		Bitmap bitmap = BitmapFactory.decodeStream(is, null, bmOptions);

		int photoW = bitmap.getWidth();
		int photoH = bitmap.getHeight();

	    int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

		/* Set bitmap options to scale the image decode target */
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor;
		bmOptions.inPurgeable = true;

		int newWidth = 160;
		int newHeight = 120;

		if(photoH>photoW){
			newWidth = 120;
			newHeight = 160;
		}

		float scaleWidth = ((float) newWidth) / photoW;
		float scaleHeight = ((float) newHeight) / photoH;

		Matrix matrix = new Matrix();
		matrix.postScale(scaleWidth, scaleHeight);

		Bitmap resizedBitmap = Bitmap.createBitmap( bitmap,0, 0,photoW, photoH, matrix, true);

		FileOutputStream ostream = null;
		try {
			if(photoW >photoH){
				matrix.preRotate(90);
				resizedBitmap = Bitmap.createBitmap( bitmap,0, 0,photoW, photoH, matrix, true);
				if(build_version > 22){
					if (ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
						ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},MY_PERMISSIONS_REQUEST_READ_EXTNL_STORE);
						ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},MY_PERMISSIONS_REQUEST_READ_EXTNL_STORE);
					}else{
						ostream = new FileOutputStream(file);
						resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
					}
				}else{
					ostream = new FileOutputStream(file);
					resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
				}

				ostream.close();

			}else{
				if(build_version > 22){
					if (ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
						ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},MY_PERMISSIONS_REQUEST_READ_EXTNL_STORE);
						ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},MY_PERMISSIONS_REQUEST_READ_EXTNL_STORE);
					}else{
						ostream = new FileOutputStream(file);
						resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
					}
				}else{
					ostream = new FileOutputStream(file);
					resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
				}
				ostream.close();
			}
		}catch(FileNotFoundException ex){
			ex.printStackTrace();
		}
				catch (IOException ex){
			ex.printStackTrace();
		}

		bitmap = resizedBitmap;

		/* Associate the Bitmap to the ImageView */
		try {
			//imageView.setImageBitmap(bitmap);
			imageView.setVisibility(View.VISIBLE);
			imageView.setImageURI(Uri.fromFile(file));
		}catch(Exception ex){
			ex.printStackTrace();
		}

        return resizedBitmap;
	}
	
	private String startCameraActivity(int actionCode){
		String fileName ="";
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		File f = null;
		try {
			f = setUpPhotoFile();
			fileName = f.getName();
			Log.d("startCameraActivity", "takenPhoto = "+fileName);
			Log.d("Context Package", "Package = "+getPackageName());
			mCurrentPhotoPath = f.getAbsolutePath();
			if(build_version >= 23){
				f.getParentFile().mkdirs();
				f.createNewFile();
				Uri contentUri = FileProvider.getUriForFile(this,getPackageName(),f);

				//takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, contentUri);
				takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
				startActivityForResult(takePictureIntent, actionCode);
			}else{
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
				startActivityForResult(takePictureIntent, actionCode);
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
			f = null;
			mCurrentPhotoPath = null;
		}
		return fileName;
	}
	
	private void getFormData(){
		String state = STATE_DOCUMENTS;
		if(id.equals("0")){
            idScanned = false;
		}

		if(photo.equals("0")){
            photoCaptured = false;
        }
		
		if(!photo.equals("0") && !id.equals("0")){
			photoCaptured = true;
            idScanned = true;
            state = STATE_ENROLLMENT;
		}

        insertIntoStateTable(state);

        startConfirmation();
	}
	
	private void startConfirmation(){
		Intent confirmationIntent = new Intent(this,ConfirmationActivity.class);
		confirmationIntent.putExtra(EnrollmentActivity2.activityKey,createConfirmationData());
		startActivity(confirmationIntent);
	}
	
	private long insertIntoStateTable(String state){
		long entryId = 0;
        ReaderDBHelper dbHelper = null;
        SQLiteDatabase db = null;
        try{
            dbHelper = new ReaderDBHelper(getBaseContext());
            db = dbHelper.getWritableDatabase();

            values.clear();
            values.put(State.EMPLOYEE_ID, employeeId);
            values.put(State.EMPLOYER_ID, employerId);
            values.put(State.DOB, dob);
            values.put(State.PHONE_NUMBER, phone);
            values.put(State.GENDER, gender);
            values.put(State.CAPTURED_ID, idScanned);
            values.put(State.CAPTURED_PHOTO, photoCaptured);
            values.put(State.STAFF_PIN, salary);
            values.put(State.NSSF_NUMBER, nssf);
            values.put(State.ACCOUNT_NUMBER, account);
            values.put(State.SITE_ID, siteId);
            values.put(State.BANK_ID, bankId);
            values.put(State.APPOINTMENT_DATE, limit);
            values.put(State.ID_FILE, id);
            values.put(State.PHOTO_FILE, photo);
            values.put(State.STATUS, state);
            values.put(State.USERNAME, MainActivity.username);
            values.put(State.PUSHER, MainActivity.password);
    	
    	// Insert the new row, returning the primary key value of the new row

    		entryId = db.insertOrThrow(State.TABLE_NAME, State.COLUMN_NAME_NULLABLE, values);
			stateId = String.valueOf(entryId);
    	}
    	catch(SQLException ex){// if exists, update row
    		String selection = State.PHONE_NUMBER + " = ?";
        	String[] selectionArgs = {phone};
    		
    		entryId = db.update(State.TABLE_NAME,values,selection,selectionArgs);
    	}catch (Exception ex){
            ex.printStackTrace();
        }
    	
    	///entryId = db.insertWithOnConflict(State.TABLE_NAME, State.COLUMN_NAME_NULLABLE, values, SQLiteDatabase.CONFLICT_IGNORE);
    	
    	Log.d(DEBUG_TAG, "New State Row Id:"+ entryId+" "+employeeId);
        Log.d(DEBUG_TAG, "EmployerID  = "+employerId);
    	values.clear();

		db.close();
		dbHelper.close();

		return entryId;
	}
	
	private File getAlbumStorageDir(String albumName) {
	    // Get the directory for the user's public files directory.
	    File file = new File(Environment.getExternalStoragePublicDirectory(
	            Environment.DIRECTORY_PICTURES), albumName);
	    if (!file.mkdirs()) {
	        Log.e(DEBUG_TAG, "Directory not created");
	    }
	    return file;
	}
	
	private void setEmployeeFields(Intent intent){
		if(intent.hasExtra(EnrollmentActivity1.activityKey)){
			employeeInfo = intent.getStringExtra(EnrollmentActivity1.activityKey);
			ArrayList<String> emppDetails = ManageServerConnections.split(employeeInfo, "*");

            Log.d(DEBUG_TAG, "Enrollment info:"+employeeInfo);

			//this.employee = emppDetails.get(1);
			this.employer = emppDetails.get(0);
			this.employerId = emppDetails.get(7);
			this.employeeId = emppDetails.get(8);
			this.dob = emppDetails.get(2);
			this.gender = emppDetails.get(3);
			this.phone = emppDetails.get(4);
			this.salary = emppDetails.get(6);
			this.limit = emppDetails.get(5);
			this.siteId = emppDetails.get(9);
			this.bankId = emppDetails.get(10);
			this.nssf = emppDetails.get(11);
			this.account = emppDetails.get(12);
		}
	}
	
	private void setIMageViews(Intent intent){
		if(intent.hasExtra(EmployerActivity.ID_FILE)){
			id = intent.getStringExtra(EmployerActivity.ID_FILE);
		}
		
		if(intent.hasExtra(EmployerActivity.PHOTO_FILE)){
			photo = intent.getStringExtra(EmployerActivity.PHOTO_FILE);
    	}
    	
		if(intent.hasExtra(EmployerActivity.ID_SCANNED)){
			String isIdScanned = intent.getStringExtra(EmployerActivity.ID_SCANNED);
    		if(isIdScanned.equals("false")){
    			idScanned = false;
                idBitmap = BitmapFactory.decodeResource(getBaseContext().getResources(),picker_icon);
    		}
    		else{
    			idScanned = true;
                idBitmap = setPicToImageView(id, idImageView);
    		}
    	}
		
		if(intent.hasExtra(EmployerActivity.PHOTO_CAPTURED)){
			String isPhotoCaptured = intent.getStringExtra(EmployerActivity.PHOTO_CAPTURED);
			if(isPhotoCaptured.equals("false")){
				photoCaptured = false;
                photoBitmap = BitmapFactory.decodeResource(this.getResources(),picker_icon);
			}
			else{
				photoCaptured = true;
                photoBitmap = setPicToImageView(photo, photoImageView);
    		}
		}
	}
	
	private String createConfirmationData(){
		String data = employeeInfo+"*"+idScanned+"*"+photoCaptured+"*"+id+"*"+photo+"*"+stateId;
		photoPath = "";
		idPath = "";
		return data;
	}
	
	public static EnrollmentActivity2 getInstance(){
		return enrollActivity;
	}
	
	private Bitmap setPicToImageView(String fileName, ImageView imageView) {
		/* There isn't enough memory to open up more than a couple camera photos */
		/* So pre-scale the target bitmap into which the file is decoded */

		/* Get the size of the ImageView */
		int targetW = imageView.getWidth();
		int targetH = imageView.getHeight();
		
		String photoPath = getAlbumStorageDir("loanAppAlbum")+"/"+fileName;
        Bitmap bitmap = null;

        System.out.println(photoPath);

		/* Get the size of the image */
		try{
			BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		
			bmOptions.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(photoPath, bmOptions);
			int photoW = bmOptions.outWidth;
			int photoH = bmOptions.outHeight;

			/* Figure out which way needs to be reduced less */
            int scaleFactor = Math.min(photoW/300, photoH/300);

			/* Set bitmap options to scale the image decode target */
			bmOptions.inJustDecodeBounds = false;
			bmOptions.inSampleSize = scaleFactor;
			bmOptions.inPurgeable = true;

			/* Decode the JPEG file into a Bitmap */
			bitmap = BitmapFactory.decodeFile(photoPath, bmOptions);

			/* Associate the Bitmap to the ImageView */
			imageView.setImageBitmap(bitmap);
			imageView.setVisibility(View.VISIBLE);
		}
		catch(Exception ex){
			Toast.makeText(EnrollmentActivity2.this, "Images not Found!", Toast.LENGTH_SHORT).show();
            ex.printStackTrace();
		}

        return bitmap;
	}

    private String getPath(Uri uri){
		Log.d("Image URI", "Photo URI = "+uri.toString());
        String[] filePathColumn = { MediaStore.Images.Media.DATA };
		Log.d("Image URI", "Media Data = "+filePathColumn.toString());
        Cursor cursor = getContentResolver().query(uri,filePathColumn, null, null, null);
		assert cursor != null;
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();

        return picturePath;
    }

    private void resizePicture(Uri picUri){

        try{
            //String newPicPath = (picUri.getPath()).replace(".jpg",".png");
            File file = new File(picUri.getPath());

            BitmapFactory.Options options=new BitmapFactory.Options();

            InputStream is = getContentResolver().openInputStream(picUri);

            Bitmap bitmap = BitmapFactory.decodeStream(is, null, options);

            if (is != null) {
                is.close();
            }

            int width = bitmap.getWidth();
            int height = bitmap.getHeight();

            int newWidth = 160;
            int newHeight = 120;

            if(height>width){
                newWidth = 120;
                newHeight = 160;
            }

            float scaleWidth = ((float) newWidth) / width;
            float scaleHeight = ((float) newHeight) / height;

            Matrix matrix = new Matrix();
            matrix.postScale(scaleWidth, scaleHeight);

            Bitmap resizedBitMap = Bitmap.createBitmap( bitmap,0, 0,width, height, matrix, true);
            boolean fileCreated = file.createNewFile();
            Log.d(DEBUG_TAG,"File created ="+fileCreated);
            FileOutputStream ostream = new FileOutputStream(file);
            resizedBitMap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
            ostream.close();

        }
        catch (IOException ex){
            ex.printStackTrace();
        }
    }

    private String extractName(String path){
		if(path != null){
			String [] files = path.split("/");
			return  files[files.length-1];
		}
        return "";
    }

	//insert picture absolute paths into pics table
	private void InsertPic(String path,int docType){
		ReaderDBHelper dbHelper = new ReaderDBHelper(getBaseContext());
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		values.clear();
		values.put(ReaderContract.Pics.MSISDN, phone);
		values.put(ReaderContract.Pics.NAME, path);
		values.put(ReaderContract.Pics.DOCUMENT_TYPE, docType);
		values.put(ReaderContract.Pics.PROJECT, employerId);
		values.put(ReaderContract.Pics.SYNCED, false);
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		values.put(ReaderContract.Pics.TS,ts.toString());
		db.insert(ReaderContract.Pics.TABLE_NAME,ReaderContract.Pics.COLUMN_NAME_NULLABLE,values);
		values.clear();
	}

    private void rotatePic(String picPath){

        try{
            File file = new File(picPath);

            BitmapFactory.Options options=new BitmapFactory.Options();

            InputStream is = getContentResolver().openInputStream(Uri.fromFile(file));

            Bitmap bitmap = BitmapFactory.decodeStream(is, null, options);

            is.close();

            int width = bitmap.getWidth();
            int height = bitmap.getHeight();

            Matrix matrix = new Matrix();
            matrix.preRotate(90);

            Bitmap rotatedBitMap = Bitmap.createBitmap( bitmap,0, 0,width, height, matrix, true);
            file.createNewFile();
            FileOutputStream ostream = new FileOutputStream(file);
            rotatedBitMap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
            ostream.close();

            onStart();
            //reportPicView.setImageBitmap(rotatedBitMap);

        }catch(FileNotFoundException ex){
            ex.printStackTrace();
        }
        catch (IOException ex){
            ex.printStackTrace();
        }

    }

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		switch (requestCode) {
			case MY_PERMISSIONS_REQUEST_READ_EXTNL_STORE: {
				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					// permission was granted, yay! Do the
					// contacts-related task you need to do.
					try{
						is = getContentResolver().openInputStream(Uri.fromFile(file));
					}catch (FileNotFoundException ex){
						ex.printStackTrace();
					}
				} else {
					// permission denied, boo! Disable the
					// functionality that depends on this permission.
					finish();
				}
				return;
			}

			// other 'case' lines to check for other
			// permissions this app might request.
		}
	}
}
