package com.peakbw.askarienrol.activities;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import com.peakbw.askarienrol.R;
import com.peakbw.askarienrol.db.ReaderContract;
import com.peakbw.askarienrol.db.ReaderDBHelper;
import com.peakbw.askarienrol.net.ManageServerConnections;

public class DetailsActivity extends Activity {

    private static final String DEBUG_TAG = "DetailsActivity";

	private String id;
    private String photo;
    private String employer;
    private String employee;
    private String gender;
    private String dob;
    private String phone;
    private String acc;
    private String nssf;
    private String limit;
	private String salary;
	private String employeeId;
	
	private TextView employerv;
	private TextView employeev;
	private TextView dobv;
	private TextView genderv;
	private TextView idScannedv;
	private TextView photoCapturedv;
	private TextView borrowingLimit;
	private TextView netSalary;
	private TextView phonev;
	private TextView accountView;
	private TextView siteView;
	private TextView bankView;
	private TextView nssfView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_details);
		// Show the Up button in the action bar.
		setupActionBar();
		
		employerv = (TextView) findViewById(R.id.employer);
        employeev = (TextView) findViewById(R.id.cemployee);
        dobv = (TextView) findViewById(R.id.dobirth);
        genderv = (TextView) findViewById(R.id.gender);
        idScannedv = (TextView) findViewById(R.id.cid);
        photoCapturedv = (TextView) findViewById(R.id.photo);
        borrowingLimit = (TextView) findViewById(R.id.bLimit);
        netSalary = (TextView) findViewById(R.id.eSalary);
        phonev = (TextView) findViewById(R.id.phone);

		accountView = (TextView) findViewById(R.id.acc);
		nssfView = (TextView) findViewById(R.id.nssf);
		bankView = (TextView) findViewById(R.id.bank);
		siteView = (TextView) findViewById(R.id.site);
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {
		assert getActionBar() != null;
		getActionBar().setDisplayHomeAsUpEnabled(true);

	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
    	}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
    protected void onStart() {
        super.onStart();
        
        Intent intent = getIntent();
		String employeeData = intent.getStringExtra(HistoryActivity.activityKey);
        System.out.println("Hustory Data = "+ employeeData);
        if(employeeData != null && employeeData.length()>0){
            setEmployeeFields(employeeData);

            employerv.setText(employer.toUpperCase());
            String emp = employee+"-"+employeeId;
            employeev.setText(emp);
            dobv.setText(dob);
            genderv.setText(gender);
            phonev.setText(phone);
            borrowingLimit.setText(limit);
            netSalary.setText(salary);
            idScannedv.setText(id);
            photoCapturedv.setText(photo);

            accountView.setText(acc);
            nssfView.setText(nssf);
            idScannedv.setText(id);
            photoCapturedv.setText(photo);
        }
    }
	
	@Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }
	
    @Override
    protected void onPause() {
        super.onPause();
    }
    
    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
    }
    
    private void setEmployeeFields(String info){
		ArrayList<String> emppDetails = ManageServerConnections.split(info, "*");
        //securityplus*James Makabugo*1970-06-19*MALE*0752360052*2015-06-19*6900*securityplus*6900*true*true*aandm_888888_photo_-433843542.jpg**aandm_888888_photo_-144225075.jpg
		this.employee = emppDetails.get(1);
		this.employer = emppDetails.get(0);
		this.employeeId = emppDetails.get(8);
		this.dob = emppDetails.get(2);
		this.gender = emppDetails.get(3);
		this.phone = emppDetails.get(4);
		this.salary = emppDetails.get(6);
		this.limit = emppDetails.get(5);
		this.id = emppDetails.get(11);
		this.photo = emppDetails.get(12);

		this.acc = emppDetails.get(15);
		this.nssf = emppDetails.get(16);

        ReaderDBHelper dbHelper = new ReaderDBHelper(getBaseContext());

        String bankId = emppDetails.get(14);

        Cursor cursor = dbHelper.getBank(bankId.trim());
        int count = cursor.getCount();
        Log.d(DEBUG_TAG,"Bank Rows : "+count);
        if(count>0){
            cursor.moveToFirst();
            String BankName = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Banks.BANK_NAME));
            bankView.setText(BankName);
        }

        String siteId = emppDetails.get(13);

        cursor = dbHelper.getSite(siteId.trim());
        count = cursor.getCount();
        Log.d(DEBUG_TAG,"Site Rows : "+count);
        if(count>0){
            cursor.moveToFirst();
            String siteName = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Sites.SITE_NAME));
            siteView.setText(siteName);
        }

        cursor.close();
        dbHelper.close();
	}

}
