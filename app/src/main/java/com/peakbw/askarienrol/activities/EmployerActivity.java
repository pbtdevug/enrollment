package com.peakbw.askarienrol.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.peakbw.askarienrol.R;
import com.peakbw.askarienrol.db.ReaderContract;
import com.peakbw.askarienrol.db.ReaderContract.Employee;
import com.peakbw.askarienrol.db.ReaderContract.Employer;
import com.peakbw.askarienrol.db.ReaderDBHelper;

import java.util.ArrayList;
import java.util.Hashtable;

public class EmployerActivity extends Activity implements OnItemSelectedListener{

	//private Handler handler = new Handler(Looper.getMainLooper());
	private Spinner employerSpinner;
	public static String employeeName,employeeId,employerId;
	private String employerName;
	private static final String DEBUG_TAG = "EmployerActivity";
	private Hashtable<String,String> employerHash;
    private static EmployerActivity employerActivity;
	public static final String EMPLOYEE = "employee";
	public static final String EMPLOYEE_ID = "employe_id";
	public static final String EMPLOYER = "employer";
	public static final String EMPLOYER_ID = "employer_id";
	public static final String DOB = "dob";
	public static final String PHONE = "p_number";
	public static final String APPOINTMENT_DATE = "appointment";
	public static final String STAFF_PIN = "pin";
	public static final String ID_FILE = "id";
	public static final String PHOTO_FILE = "photo";

    public static final String ACCOUNT = "acc";
    public static final String NSSF = "nssf";
    public static final String BANK_ID = "bankId";
    public static final String SITE_ID = "siteId";
	
	public static final String ID_SCANNED = "id_scanned";
	public static final String PHOTO_CAPTURED = "photo_captured";

    private EditText idField,firstNameField,surnameField;
    //private TextView idLabel;

    public static final int DIALOG_URL = 3;

    private ContentValues values;

    public static boolean newStaff = true;


    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_employer);
		// Show the Up button in the action bar.
		setupActionBar();
		
		setTitle("Staff Details");

        ReaderDBHelper dbHelper = new ReaderDBHelper(this);
		
        //run query that returns all employers MainActivity.rDbHelper
        Cursor cursor = dbHelper.getAllEmpoyers();
        
        ArrayList<String> employerList = new ArrayList<>();
        
        employerHash = new Hashtable<>();
        
        while(cursor.moveToNext()){
        	String employer = cursor.getString(cursor.getColumnIndexOrThrow(Employer.EMPLOYER_NAME));
        	String employerId = cursor.getString(cursor.getColumnIndexOrThrow(Employer.EMPLOYER_ID));
        	employerHash.put(employer, employerId);
        	employerList.add(employer);
        }

        cursor.close();

        idField = (EditText) findViewById(R.id.employeeId);
        idField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if((id == R.id.continu || id == EditorInfo.IME_NULL) && newStaff){
                    validate();
                    return true;
                }
                return false;
            }
        });

        TextWatcher textWatcher = new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int inputLength = s.length();
                if(inputLength>0){
                    setNames(s.toString(),employerName);
                }

            }

            public void afterTextChanged(Editable s) {}
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        };

        idField.addTextChangedListener(textWatcher);

        firstNameField = (EditText) findViewById(R.id.firstName);
        firstNameField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if(id == R.id.continu || id == EditorInfo.IME_NULL){
                    validate();
                    return true;
                }
                return false;
            }
        });

        surnameField = (EditText) findViewById(R.id.surName);
        surnameField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if(id == R.id.continu || id == EditorInfo.IME_NULL){
                    validate();
                    return true;
                }
                return false;
            }
        });

        //idLabel = (TextView) findViewById(R.id.staffId);

        employerSpinner = (Spinner) findViewById(R.id.employerSpinner);
        employerSpinner.setOnItemSelectedListener(this);
        
        ArrayAdapter<String> employersAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,employerList);
        employersAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        employerSpinner.setAdapter(employersAdapter);

        //get db instance
        values = new ContentValues();

        //LinearLayout container = (LinearLayout) findViewById(R.id.names);

        //LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //View new_view = layoutInflater.inflate(R.layout.new_staff, null);
        //container.addView(new_view);

        employerActivity = this;
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {
		assert getActionBar() != null;
        getActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	
	@Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
        Log.d(DEBUG_TAG,"URL = "+MainActivity.url);
        Log.d(DEBUG_TAG,"newStaff Value = "+newStaff);
    }
	
	@Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }
    @Override
    protected void onPause() {
        super.onPause();
      //time out the
        /*handler.postDelayed(new Runnable() {
        	@Override
        	public void run() {
        		finish();
      			}
        }, 120000 );*/
        // Another activity is taking focus (this activity is about to be "paused").
    }
    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
    }

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
		if(parent.equals(employerSpinner)){
			employerName = (String)parent.getItemAtPosition(pos);
		}
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.employer, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		
		case R.id.action_history:
			Intent histIntent = new Intent(this, HistoryActivity.class);
	        startActivity(histIntent);
			return true;

        /*case R.id.action_url:
            showDialog(DIALOG_URL);
            return true;*/
		
		case android.R.id.home:
			finish();
			return true;

    }
		
		return super.onOptionsItemSelected(item);
	}
	
	public void cancel(View view){

        finish();
	}

    public void onRadioButtonClick(View view){
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.new_option:
                if (checked)
                    //if(!newStaff){
                        //idLabel.setVisibility(View.GONE);
                        idField.setVisibility(View.GONE);
                    //}
                    newStaff = true;
                    Log.d(DEBUG_TAG,"Staff New = "+ newStaff);
                break;
            case R.id.exist_option:
                if (checked){
                    //idLabel.setVisibility(View.VISIBLE);
                    idField.setVisibility(View.VISIBLE);
                    Log.d(DEBUG_TAG,"Staff New = "+ newStaff);
                    newStaff = false;
                }
                break;
        }
    }

	public void getEmployeeDetails(View view){
        validate();
	}

	private void validate(){
        String firstName = firstNameField.getText().toString();
        String surname = surnameField.getText().toString();
        String id = "";

        Log.d(DEBUG_TAG, "First Name = "+firstName+"\nSurname = "+surname);

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(firstName)){
            firstNameField.setError(getString(R.string.error_fname_required));
            focusView = firstNameField;
            cancel = true;
        }

        if (TextUtils.isEmpty(surname)){
            surnameField.setError(getString(R.string.error_sname_required));
            focusView = surnameField;
            cancel = true;
        }

        if(!newStaff){
            id = idField.getText().toString();
            if (TextUtils.isEmpty(id)){
                idField.setError(getString(R.string.error_id_required));
                focusView = idField;
                cancel = true;
            }
        }

        if(cancel){
            focusView.requestFocus();
        }else {
            idField.setText("");
            surnameField.setText("");
            firstNameField.setText("");

            employeeName = firstName.toUpperCase()+" "+surname.toUpperCase();
            employeeId = "0";

            if(!newStaff){
                Log.d(DEBUG_TAG, "EmployeeID = "+id);
                Log.d(DEBUG_TAG, "EmployerName = "+employerName);

                ReaderDBHelper dbHelper = new ReaderDBHelper(this);

                Cursor cursor = dbHelper.getEmployeeName(Integer.parseInt(id),employerName);

                int count = cursor.getCount();
                Log.d(DEBUG_TAG, "Row Count  = "+count);
                if(count>0){
                    cursor.moveToFirst();

                    employeeName = cursor.getString(cursor.getColumnIndexOrThrow(Employee.EMPLOYEE_NAME));
                    employeeId = id;
                }

                cursor.close();
                dbHelper.close();
            }

            startEnrollmentActivity1();
        }
    }

    private void setNames(String id,String employer){
        ReaderDBHelper dbHelper = new ReaderDBHelper(this);

        Cursor cursor = dbHelper.getEmployeeName(Integer.parseInt(id),employer);

        int count = cursor.getCount();
        Log.d(DEBUG_TAG, "Row Count  = "+count+"\nUser Id :"+id+"\nEmployer : "+employer+"\n");
        firstNameField.setText("");
        surnameField.setText("");
        if(count>0){
            cursor.moveToFirst();

            String name = cursor.getString(cursor.getColumnIndexOrThrow(Employee.EMPLOYEE_NAME));
            String [] names = name.split(" ");
            firstNameField.setText(names[0]);
            surnameField.setText(names[1]);
        }

        cursor.close();
        dbHelper.close();
    }
	
	private void startEnrollmentActivity1(){
		Intent enrollIntent = new Intent(this,EnrollmentActivity1.class);
		enrollIntent.putExtra(EMPLOYEE,employeeName);
		enrollIntent.putExtra(EMPLOYEE_ID,employeeId);
		enrollIntent.putExtra(EMPLOYER,employerName);
		enrollIntent.putExtra(EMPLOYER_ID,employerHash.get(employerName));
		enrollIntent.putExtra(DOB,"");
		enrollIntent.putExtra(PHONE,"");
		enrollIntent.putExtra(APPOINTMENT_DATE,"");
		enrollIntent.putExtra(STAFF_PIN,"");

        enrollIntent.putExtra(ACCOUNT,0);
        enrollIntent.putExtra(NSSF,0);
        enrollIntent.putExtra(SITE_ID,"");
        enrollIntent.putExtra(BANK_ID,"");
		
		enrollIntent.putExtra(EmployerActivity.ID_FILE,"0");
		enrollIntent.putExtra(EmployerActivity.PHOTO_FILE,"0");
		
		enrollIntent.putExtra(EmployerActivity.ID_SCANNED,"false");
		enrollIntent.putExtra(EmployerActivity.PHOTO_CAPTURED,"false");
		startActivity(enrollIntent);
	}

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {

            case DIALOG_URL:
                AlertDialog.Builder urlDialog = new AlertDialog.Builder(this);
                LayoutInflater li = LayoutInflater.from(this);
                View promptsView = li.inflate(R.layout.url_prompot, null);
                final EditText urlInput = (EditText) promptsView.findViewById(R.id.urlField);
                urlInput.setText(MainActivity.url);
                urlDialog.setView(promptsView);
                urlDialog.setCancelable(false);
                urlDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    // do something when the button is clicked
                    public void onClick(DialogInterface dialog, int id) {
                        String urlString = urlInput.getText().toString();
                        if(urlString.length()>=20){

                            ReaderDBHelper dbHelper = new ReaderDBHelper(getBaseContext());
                            SQLiteDatabase db = dbHelper.getReadableDatabase();

                            MainActivity.url = urlString;
                            //update table signature
                            values.put(ReaderContract.Signature.UPDATE_URL, urlString);
                            //excute query
                            int count = db.update(ReaderContract.Signature.TABLE_NAME,values,null,null);
                            //
                            Log.d(DEBUG_TAG, "New URL= : "+count+" "+urlString);
                            values.clear();

                            db.close();
                            dbHelper.close();
                        }
                        else{
                            Toast.makeText(EmployerActivity.this,"Invalid URL!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                urlDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                return urlDialog.create();

            default:
                return null;
        }
    }

    /*private void createAlertDialog(String msg,int layout,int textView){
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(layout, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);

        final TextView alertTextView = (TextView) promptsView.findViewById(textView);
        alertTextView.setText(msg);

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                newStaff = true;
                View View = layoutInflater.inflate(R.layout.new_staff, null);
                container.addView(View);
                // Disable new staff item

            }
        }).setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }*/

    public static EmployerActivity getInstance(){
        return employerActivity;
    }

    /*private void setNames(String input){
        ReaderDBHelper dbHelper = new ReaderDBHelper(this);

        Cursor cursor = dbHelper.getEmployeeName(Integer.parseInt(input),employerName);

        int count = cursor.getCount();
        Log.d(DEBUG_TAG, "Row Count  = "+count);
        if(count>0){
            cursor.moveToFirst();
            String name = cursor.getString(cursor.getColumnIndexOrThrow(Employee.EMPLOYEE_NAME));

            Log.d(DEBUG_TAG, "Name For ID = "+input+" : "+name);

            String [] namesArray = name.split(" ");
            employeeName = name;
            employeeId = input;

            firstNameField.setText(namesArray[0]);
            surnameField.setText(namesArray[1]);
        }
    }*/
}
