package com.peakbw.askarienrol.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.peakbw.askarienrol.R;
import com.peakbw.askarienrol.db.ReaderContract.Signature;
import com.peakbw.askarienrol.db.ReaderDBHelper;
import com.peakbw.askarienrol.net.ManageServerConnections;
import com.peakbw.askarienrol.net.XMLParser;
import com.peakbw.askarienrol.services.FTPService;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends Activity {
	
	//private Handler handler = new Handler(Looper.getMainLooper());
	private static final String DEBUG_TAG = "MainActivity";
    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    public static final int DIALOG_DOWNLOAD_PROGRESS1 = 2;
    public static final int DIALOG_URL = 3;
    private ProgressDialog mProgressDialog;
    public static ProgressDialog dProgressDialog;
	private  ManageServerConnections mdc;
    private String signature;
    //public static String phoneID,simSerialNumber/*,simNumber*/;
    public static String date,activeEmployee = "";
    public static String password,username,versionName;
    public static  int lenghtOfFile;
    private ContentValues values;
    private EditText passwordField,usernameFiled;
    public static String url;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
        
        date = getDate();
        mdc = new ManageServerConnections();

        values = new ContentValues();

		Intent serviceIntent = new Intent(this, FTPService.class);
		startService(serviceIntent);
	}
	
	@Override
	public void onStart(){
		super.onStart();

		passwordField = (EditText) findViewById(R.id.user_pini);
		passwordField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
				if (id == R.id.login || id == EditorInfo.IME_NULL) {
					validate();
					return true;
				}
				return false;
			}
		});

		usernameFiled = (EditText) findViewById(R.id.username);
		usernameFiled.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
				if (id == R.id.login || id == EditorInfo.IME_NULL) {
					validate();
					return true;
				}
				return false;
			}
		});



		ReaderDBHelper dbHelper = new ReaderDBHelper(getBaseContext());
		SQLiteDatabase db = dbHelper.getWritableDatabase();

        // get updateSignature and connection url
        Cursor cursor = dbHelper.getSignature();
        cursor.moveToFirst();
        signature = cursor.getString(cursor.getColumnIndexOrThrow(Signature.SIGNATURE));
        url = cursor.getString(cursor.getColumnIndexOrThrow(Signature.UPDATE_URL));
        Log.d(DEBUG_TAG, "URL = "+url);

        if(url.equals("url")){
			url = getString(R.string.connUrl);
			//update table signature
			values.put(Signature.UPDATE_URL, url);
			//excute query
			int count = db.update(Signature.TABLE_NAME,values,null,null);
			//
			Log.d(DEBUG_TAG, "New URL= : "+count+" "+url);
			values.clear();
            //showDialog(DIALOG_URL);
        }
		db.close();
		dbHelper.close();
	}

    public void login(View view){
        validate();
    }

	private void validate(){
		password = passwordField.getText().toString();
		username = usernameFiled.getText().toString();

		Log.d(DEBUG_TAG, "Username = "+username+"\nPassword = "+password);

		boolean cancel = false;
		View focusView = null;

		if (TextUtils.isEmpty(password)){
			passwordField.setError(getString(R.string.error_password_required));
			focusView = passwordField;
			cancel = true;
		}

		if (TextUtils.isEmpty(username)){
			usernameFiled.setError(getString(R.string.error_username_required));
			focusView = usernameFiled;
			cancel = true;
		}

		if(cancel){
			focusView.requestFocus();
		}else {
			usernameFiled.setText("");
			passwordField.setText("");

			String request = createUpdateXML(password,username, signature);
			mdc.setUpdateVariable(request);
			new DownloadXMLTask().execute(url);
		}
	}
	
	private class DownloadXMLTask extends AsyncTask<String, String, String> {
		private String status = "";
		private InputStream stream = null;
		private static final String DEBUG_TAG = "DownloadXMLTask";
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		@SuppressWarnings("deprecation")
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(DIALOG_DOWNLOAD_PROGRESS);
		}

		@Override
		protected String doInBackground(String... url) {
			try {
	    		stream = mdc.downloadUrl(url[0]);
	    		if(stream!=null){
	    		try	{
	    			
	    			byte[] buffer = new byte[1024];
	                int len;
	                long total = 0;
	                
	                while ((len = stream.read(buffer)) > -1) {
	                	total += len;
	                	// After this onProgressUpdate will be called
	                	
	                    publishProgress(""+(int)((total*100)/lenghtOfFile));
	                    //Log.d("ANDRO_ASYNC","length ="+lenghtOfFile);
	                    baos.write(buffer, 0, len);
	                }
	                baos.flush();
	                baos.close();
	                stream.close();
	    			
	    			status = XMLParser.parseXML(new ByteArrayInputStream(baos.toByteArray()),getBaseContext());
	    		}
	    		catch(Exception ex){
	    			ex.printStackTrace();
	    		}
	    		}
	    	}
	    	catch(IOException ex){
	        	ex.printStackTrace();
	        }
	        finally {
	            if (stream != null) {
	            	try{
	            		stream.close();
	                }catch(IOException ex){
	                	ex.printStackTrace();
	                }
	            }
	        }
	    	return status;
		}
		
		protected void onProgressUpdate(String... progress) {
			 //Log.d("ANDRO_ASYNC",progress[0]);
			 mProgressDialog.setProgress(Integer.parseInt(progress[0]));
		}

		@SuppressWarnings("deprecation")
		@Override
	    protected void onPostExecute(String result) {
			dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
	    	if(result!=null){
	    		Log.d(DEBUG_TAG, "result = "+result);
				if(!result.equalsIgnoreCase("SUCCESSFUL")){
					usernameFiled.setError(result);
					usernameFiled.requestFocus();
					return;
				}
				//start EmployerActivity
				startEmployerActivity();
	    	}else{
				usernameFiled.setError("No internet access");
				usernameFiled.requestFocus();
			}
	    }
	}
	
	private String createUpdateXML(String password,String username, String signature){
		StringBuilder xmlStr = new StringBuilder();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<pbtRequest>");
        xmlStr.append("<password>").append(password).append("</password>");
		xmlStr.append("<username>").append(username).append("</username>");
        xmlStr.append("<method>").append("askari_update").append("</method>");
        xmlStr.append("<signature>").append(signature).append("</signature>");
        xmlStr.append("<appVersion>").append(versionName).append("</appVersion>");
        xmlStr.append("</pbtRequest>");
        System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }
	
	private String getDate() {//Fri 29 Nov 2013
        ArrayList<String> dateInfo = ManageServerConnections.split((new Date()).toString()," ");
		return dateInfo.get(2) +" "+dateInfo.get(1)+" "+dateInfo.get(5);
    }
	
	private void startEmployerActivity(){
		Intent employerIntent = new Intent(this,EmployerActivity.class);
		startActivity(employerIntent);
		finish();
	}
	
	@Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }
    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
    }
    
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
			case DIALOG_DOWNLOAD_PROGRESS:
				mProgressDialog = new ProgressDialog(this);
				mProgressDialog.setMessage("please wait");
				mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				mProgressDialog.setCancelable(true);
				mProgressDialog.show();
				
				return mProgressDialog;
				
			case DIALOG_DOWNLOAD_PROGRESS1:
				dProgressDialog = new ProgressDialog(this);
				dProgressDialog.setMessage("dowloading apk..");
				dProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				dProgressDialog.setCancelable(true);
				
				return dProgressDialog;

            case DIALOG_URL:
				AlertDialog.Builder urlDialog = new AlertDialog.Builder(this);
                LayoutInflater li = LayoutInflater.from(this);
                View promptsView = li.from(getApplicationContext()).inflate(R.layout.url_prompot, null);
                final EditText urlInput = (EditText) promptsView.findViewById(R.id.urlField);
                urlDialog.setView(promptsView);
                urlDialog.setCancelable(false);
                urlDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    // do something when the button is clicked
                    public void onClick(DialogInterface dialog, int id) {
                        String urlString = urlInput.getText().toString();
                        if(urlString.length()>=20){

							ReaderDBHelper dbHelper = new ReaderDBHelper(getBaseContext());
							SQLiteDatabase db = dbHelper.getWritableDatabase();

                            url = urlString;
                            //update table signature
                            values.put(Signature.UPDATE_URL, url);
                            //excute query
                            int count = db.update(Signature.TABLE_NAME,values,null,null);
                            //
                            Log.d(DEBUG_TAG, "New URL= : "+count+" "+url);
                            values.clear();

							db.close();
							dbHelper.close();
                        }
                        else{
                            Toast.makeText(MainActivity.this,"Invalid URL!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                urlDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                return urlDialog.create();
				
			default:
				return null;
        }
    }
}
