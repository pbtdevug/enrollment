package com.peakbw.askarienrol.db;

import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.peakbw.askarienrol.activities.EnrollmentActivity2;
import com.peakbw.askarienrol.db.ReaderContract.Auth;
import com.peakbw.askarienrol.db.ReaderContract.Employee;
import com.peakbw.askarienrol.db.ReaderContract.Employer;
import com.peakbw.askarienrol.db.ReaderContract.Signature;
import com.peakbw.askarienrol.db.ReaderContract.State;

public class ReaderDBHelper extends SQLiteOpenHelper {
	
	// If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "askariEnroldatabase.db";
    public static SQLiteDatabase mDB;
    public static final String DEBUG_TAG = "ReaderDBHelper";
	private static final String TEXT_TYPE = " TEXT";
	private static final String DATE = " DATE";
	private static final String TIMESTAMP = " TIMESTAMP";
	private static final String COMMA_SEP = ",";
	private static ContentValues values = new ContentValues();
    
    private static final String SQL_CREATE_EMPLOYEE =
    	    "CREATE TABLE " + Employee.TABLE_NAME + " (" +
    	    		Employee._ID + " INTEGER PRIMARY KEY," + 
    	    		Employee.EMPLOYEE_ID +TEXT_TYPE +" UNIQUE NULL," +
    	    		Employee.EMPLOYEE_NAME + TEXT_TYPE + COMMA_SEP+
					Employee.STATE_ID + TEXT_TYPE + COMMA_SEP+
    	    		Employee.EMPLOYER_ID + TEXT_TYPE+ ")";
    
    private static final String SQL_DELETE_EMPLOYEE ="DROP TABLE IF EXISTS " + Employee.TABLE_NAME;
    
    private static final String SQL_CREATE_EMPLOYER =
    	    "CREATE TABLE " + Employer.TABLE_NAME + " (" +
    	    		Employer._ID + " INTEGER PRIMARY KEY," +
    	    		Employer.EMPLOYER_ID +TEXT_TYPE+ " UNIQUE NOT NULL," +
    	    		Employer.EMPLOYER_NAME + TEXT_TYPE + ")";
    
    private static final String SQL_DELETE_EMPLOYER ="DROP TABLE IF EXISTS " + Employer.TABLE_NAME;

	private static final String SQL_CREATE_BANKS =
			"CREATE TABLE " + ReaderContract.Banks.TABLE_NAME + " (" +
					ReaderContract.Banks._ID + " INTEGER PRIMARY KEY," +
					ReaderContract.Banks.BANK_ID +TEXT_TYPE+ " UNIQUE NOT NULL," +
					ReaderContract.Banks.BANK_NAME + TEXT_TYPE + ")";

	private static final String SQL_DELETE_BANKS ="DROP TABLE IF EXISTS " + ReaderContract.Banks.TABLE_NAME;

	private static final String SQL_CREATE_SITES =
			"CREATE TABLE " + ReaderContract.Sites.TABLE_NAME + " (" +
					ReaderContract.Sites._ID + " INTEGER PRIMARY KEY," +
					ReaderContract.Sites.SITE_ID +TEXT_TYPE+ " UNIQUE NOT NULL," +
					ReaderContract.Sites.SITE_NAME + TEXT_TYPE + ")";

	private static final String SQL_DELETE_SITES ="DROP TABLE IF EXISTS " + ReaderContract.Sites.TABLE_NAME;
    
    private static final String SQL_CREATE_AUTH =
    	    "CREATE TABLE " + Auth.TABLE_NAME + " (" +
    	    		Auth._ID + " INTEGER PRIMARY KEY," + 
    	    		Auth.USER_NAME + " TEXT UNIQUE NOT NULL," +
    	    		Auth.USER_PIN + TEXT_TYPE + ")";
    
    private static final String SQL_DELETE_AUTH ="DROP TABLE IF EXISTS " + Auth.TABLE_NAME;
    
    private static final String SQL_CREATE_STATE =
    	    "CREATE TABLE " + State.TABLE_NAME + " (" +
    	    		State._ID + " INTEGER PRIMARY KEY NOT NULL," +
    	    		State.EMPLOYEE_ID + TEXT_TYPE + COMMA_SEP+
    	    		State.EMPLOYER_ID +  TEXT_TYPE + COMMA_SEP+
    	    		State.PHONE_NUMBER + TEXT_TYPE + " UNIQUE NOT NULL,"+
    	    		State.DOB + DATE + COMMA_SEP +
    	    		State.GENDER + TEXT_TYPE + COMMA_SEP +
    	    		State.CAPTURED_ID + " BOOLEAN,"+
    	    		State.SITE_ID +"  VARCHAR(45),"+
    	    		State.CAPTURED_PHOTO + " BOOLEAN," +
    	    		State.STAFF_PIN + " INTEGER," +
    	    		State.APPOINTMENT_DATE + " DATE," +
    	    		State.ID_FILE + TEXT_TYPE + COMMA_SEP +
    	    		State.BANK_ID + " VARCHAR(45)" + COMMA_SEP +
					State.ACCOUNT_NUMBER + " VARCHAR(45)" + COMMA_SEP +
					State.NSSF_NUMBER + " VARCHAR(45)" + COMMA_SEP +
    	    		State.PHOTO_FILE + TEXT_TYPE + COMMA_SEP +
    	    		State.TIME_STAMP + TIMESTAMP + COMMA_SEP +
    	    		State.STATUS + TEXT_TYPE + COMMA_SEP +
					State.USERNAME+ TEXT_TYPE + COMMA_SEP +
    	    		State.PUSHER + TEXT_TYPE +")";
    
    private static final String SQL_DELETE_STATE ="DROP TABLE IF EXISTS " + State.TABLE_NAME;

    private static final String SQL_CREATE_SIGNATURE =
    	    "CREATE TABLE " + Signature.TABLE_NAME + " (" +
    	    		Signature._ID + " INTEGER PRIMARY KEY," + 
    	    		Signature.SIGNATURE + TEXT_TYPE + COMMA_SEP +
                    Signature.UPDATE_URL+ TEXT_TYPE + COMMA_SEP +
    	    		Signature.DATE + TEXT_TYPE + ")";
    
    private static final String SQL_DELETE_SIGNATURE ="DROP TABLE IF EXISTS " + Signature.TABLE_NAME;

	private static final String SQL_CREATE_PICS =
			"CREATE TABLE " + ReaderContract.Pics.TABLE_NAME + " (" +
					ReaderContract.Pics._ID + " INTEGER PRIMARY KEY," +
					ReaderContract.Pics.NAME + TEXT_TYPE + ", "+
					ReaderContract.Pics.DOCUMENT_TYPE + " INTEGER" + ", "+
					ReaderContract.Pics.PROJECT + TEXT_TYPE + COMMA_SEP +
					ReaderContract.Pics.MSISDN + " INTEGER," +
					ReaderContract.Pics.TS + " DATETIME," +
					ReaderContract.Pics.SYNCED + " BOOLEAN "+")";

	private static final String SQL_DELETE_PICS ="DROP TABLE IF EXISTS " + ReaderContract.Pics.TABLE_NAME;
    
    public ReaderDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		ReaderDBHelper.mDB = getWritableDatabase();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		//String pin = MainActivity.pin;
		db.execSQL(SQL_CREATE_EMPLOYEE);
		Log.d(DEBUG_TAG, "SQL for Employee:"+ SQL_CREATE_EMPLOYEE);
		db.execSQL(SQL_CREATE_EMPLOYER);
		Log.d(DEBUG_TAG, "SQL for Employer:"+ SQL_CREATE_EMPLOYER);
		db.execSQL(SQL_CREATE_AUTH);
		Log.d(DEBUG_TAG, "SQL for Auth:"+ SQL_CREATE_AUTH);

		db.execSQL(SQL_CREATE_BANKS);
		Log.d(DEBUG_TAG, "SQL for Banks :"+ SQL_CREATE_BANKS);
		db.execSQL(SQL_CREATE_SITES);
		Log.d(DEBUG_TAG, "SQL for Sites:"+ SQL_CREATE_SITES);
		//insertFirstUser(pin);
		db.execSQL(SQL_CREATE_STATE);
		Log.d(DEBUG_TAG, "SQL for State:"+ SQL_CREATE_STATE);
		db.execSQL(SQL_CREATE_SIGNATURE);
		Log.d(DEBUG_TAG, "SQL for Signature:"+ SQL_CREATE_SIGNATURE);
		db.execSQL(SQL_CREATE_PICS);
		Log.d(DEBUG_TAG, "SQL for PICS:" + SQL_CREATE_PICS);
		insertFirstSignature(db);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(SQL_DELETE_EMPLOYEE);
		db.execSQL(SQL_DELETE_EMPLOYER);
		db.execSQL(SQL_DELETE_AUTH);
		db.execSQL(SQL_DELETE_STATE);
		db.execSQL(SQL_DELETE_SIGNATURE);
		db.execSQL(SQL_DELETE_PICS);
		db.execSQL(SQL_DELETE_SITES);
		db.execSQL(SQL_DELETE_BANKS);
		onCreate(db);
	}
	
	/** Returns all the employers in the table */
    public Cursor getAllEmpoyers(){
    	return mDB.query(Employer.TABLE_NAME, new String[] {Employer._ID, Employer.EMPLOYER_ID,Employer.EMPLOYER_NAME} ,
                            null, null, null, null,
                            Employer.EMPLOYER_NAME + " asc ");
    }
    
    /** Returns all the employers in the table */
    /*public Cursor getAllEmpoyees(){
    	return mDB.query(Employee.TABLE_NAME, new String[] {Employee._ID, Employee.EMPLOYEE_ID,Employee.EMPLOYEE_NAME,Employee.EMPLOYER_ID} ,
                            null, null, null, null,
                            Employee.EMPLOYEE_NAME + " asc ");
    }*/
    
    public Cursor getSignature(){
    	return mDB.query(Signature.TABLE_NAME, new String[] {Signature._ID, Signature.SIGNATURE,Signature.UPDATE_URL} ,
    			null, null, null, null,null);
    }
    
    public Cursor getEmployerID(String employeeID){
    	    	
    	return mDB.rawQuery("SELECT employerId FROM state WHERE employeeId = ?;", new String[]{employeeID});
    }
    
    public Cursor getEmployeeName(int id,String employer){
    	//Cursor c = mDB.query(Employee.TABLE_NAME, new String[] {Employee._ID, Employee.EMPLOYEE_NAME} ,
    			//Employee.EMPLOYEE_ID+" = ?", new String[]{String.valueOf(id)} , null,null,null);
    	
    	//return mDB.rawQuery("SELECT ee.employeeName FROM employee ee JOIN employer er ON ee.employerId = er.id WHERE ee.id = ? LIMIT 1;", new String[]{String.valueOf(id)}); //er.employerName = ? OR employer,
		return mDB.rawQuery("SELECT ee.employeeName FROM employee ee WHERE ee.id = ?", new String[]{String.valueOf(id)});
    }
    
    public Cursor getHistory(){
    	return mDB.rawQuery("SELECT s.employeeId,s._id,s.state,s.employerId,e.employeeName FROM state s JOIN employee e ON s._id = e.stateId;", null);
    }

	public Cursor getEmployerId(String id){
		return mDB.rawQuery("SELECT s.employerId FROM state s WHERE s._id = ?;", new String[]{id});
	}
    
    public Cursor getEmployeeHistory(String employeeID,String employerID){
    	return mDB.rawQuery("SELECT e.employerName, ee.employeeName,ee.id,e.id,s.employerId,s.employeeId,s.phoneNumber,s.dob,s.gender,s.netSalary,s.borrowingLimit FROM employee ee JOIN state s ON ee.id = s.employeeId JOIN employer e ON e.id = ee.employerId WHERE ee.id = ? AND ee.employerId = ?;", new String[]{employeeID,employerID});
    }

    public Cursor getCredentials(String phone){
        return mDB.rawQuery("SELECT pusher,username FROM state WHERE phoneNumber = ?;", new String[]{phone});
    }
    
    public Cursor getEnrollmentInfo(String _id,String employerID){
    	return mDB.rawQuery("SELECT e.employerName,s._id,s.employeeId,s.phoneNumber,s.dob,s.gender,s.pin,s.appointmentDate,s.capturedId,s.capturedPhoto,s.idFile,s.photoFile,s.state,s.acc,s.nssf,s.bankid,s.siteid FROM state s JOIN employer e ON e.id = s.employerId WHERE s."+State._ID+" = ? AND s.employerId = ?;", new String[]{_id,employerID});
    }
    
    private void insertFirstSignature(SQLiteDatabase db){
    	values.put(Signature.SIGNATURE, "0");
    	values.put(Signature.DATE, (new Date()).toString());
        values.put(Signature.UPDATE_URL, "url");
    	
    	// Insert the new row, returning the primary key value of the new row
    	long newRowId = db.insertWithOnConflict(
    			Signature.TABLE_NAME,
    			Signature.COLUMN_NAME_NULLABLE,
    			values,
    			SQLiteDatabase.CONFLICT_REPLACE);
    	Log.d(DEBUG_TAG, "New Signature Row Id:"+ newRowId+" "+"0");
    	Log.d(DEBUG_TAG, "New Signature: "+ "Signature"+"0");
    	values.clear();
    }

    public Cursor checkPhoneNumber(String phoneNumber){
    	return mDB.rawQuery("SELECT ee.employeeName,ee.id FROM employee ee JOIN state s ON ee.id = s.employeeId WHERE s.phoneNumber = ?;", new String[]{phoneNumber});
    }

	public  Cursor getPics(){
		return mDB.rawQuery("SELECT name,project FROM pics WHERE synced = 0;", null);
	}

	public  Cursor getSites(){
		return mDB.rawQuery("SELECT * FROM sites;", null);
	}

	public  Cursor getBanks(){
		return mDB.rawQuery("SELECT * FROM banks;", null);
	}

	public  Cursor getSite(String SiteId){
        return mDB.rawQuery("SELECT SiteName FROM sites WHERE SiteId = ?;", new String[]{SiteId});
	}

	public  Cursor getBank(String BankId){
		return mDB.rawQuery("SELECT BankName FROM banks WHERE BankId = ?;",new String[]{BankId});
	}

	public Cursor getFailedRecords(){
		return mDB.rawQuery("SELECT s._id,s.state,s.employerId,s.employeeId,s.phoneNumber,s.pin,s.dob,gender,s.appointmentDate,s.idFile,s.photoFile,s.nssf,s.bankid,s.acc,s.siteid,e.employeeName,er.employerName FROM state s JOIN employee e ON s._id = e.stateId JOIN employer er ON s.employerId = er.id WHERE s.state = ? ORDER BY timeStamp ASC;", new String[]{EnrollmentActivity2.STATE_ENROLLMENT});
	}

	public Cursor getUserCredentials(){
		return mDB.rawQuery("SELECT * FROM auth ;", null);
	}
}
