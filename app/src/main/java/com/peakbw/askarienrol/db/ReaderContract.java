package com.peakbw.askarienrol.db;

import android.provider.BaseColumns;

public final class ReaderContract {
	
	public ReaderContract() {
	}
	
/* Inner classes that define the table contents */
	
    public static abstract class Employee implements BaseColumns {
        public static final String TABLE_NAME = "employee";
        public static final String EMPLOYEE_ID = "id";
        public static final String EMPLOYEE_NAME = "employeeName";
        public static final String EMPLOYER_ID = "employerId";
        public static final String STATE_ID = "stateId";
        public static final String COLUMN_NAME_NULLABLE = "null";
    }
    
    public static abstract class Employer implements BaseColumns {
        public static final String TABLE_NAME = "employer";
        public static final String EMPLOYER_ID = "id";
        public static final String EMPLOYER_NAME = "employerName";
        public static final String COLUMN_NAME_NULLABLE = "null";
    }

    public static abstract class Sites implements BaseColumns {
        public static final String TABLE_NAME = "sites";
        public static final String SITE_ID = "SiteId";
        public static final String SITE_NAME = "SiteName";
        public static final String COLUMN_NAME_NULLABLE = "null";
    }

    public static abstract class Banks implements BaseColumns {
        public static final String TABLE_NAME = "banks";
        public static final String BANK_ID = "BankId";
        public static final String BANK_NAME = "BankName";
        public static final String COLUMN_NAME_NULLABLE = "null";
    }
    
    public static abstract class Auth implements BaseColumns {
        public static final String TABLE_NAME = "auth";
        public static final String USER_PIN = "pin";
        public static final String USER_NAME= "name";
        public static final String COLUMN_NAME_NULLABLE = "null";
    }
    
    public static abstract class Signature implements BaseColumns {
        public static final String TABLE_NAME = "signature";
        public static final String SIGNATURE = "updateSignature";
        public static final String DATE= "date";
        public static final String UPDATE_URL= "url";
        public static final String COLUMN_NAME_NULLABLE = "null";
    }
    
    public static abstract class State implements BaseColumns {
        public static final String TABLE_NAME = "state";
        public static final String EMPLOYER_ID = "employerId";
        public static final String EMPLOYEE_ID = "employeeId";
        public static final String PHONE_NUMBER = "phoneNumber";
        public static final String DOB = "dob";
        public static final String GENDER = "gender";
        public static final String CAPTURED_ID = "capturedId";
        public static final String CAPTURED_PHOTO = "capturedPhoto";
        public static final String STAFF_PIN = "pin";
        public static final String APPOINTMENT_DATE = "appointmentDate";
        public static final String ID_FILE = "idFile";
        public static final String PHOTO_FILE = "photoFile";
        public static final String NSSF_NUMBER = "nssf";
        public static final String BANK_ID = "bankid";
        public static final String ACCOUNT_NUMBER = "acc";
        public static final String SITE_ID = "siteid";
        public static final String TIME_STAMP = "timeStamp";
        public static final String PUSHER = "pusher";
        public static final String USERNAME = "username";
        public static final String STATUS = "state";
        public static final String COLUMN_NAME_NULLABLE = "null";
    }

    public static abstract class Pics implements BaseColumns {
        public static final String TABLE_NAME = "pics";
        public static final String NAME = "name";
        public static final String MSISDN = "msisdn";
        public static final String PROJECT= "project";
        public static final String DOCUMENT_TYPE = "doc_type";
        public static final String SYNCED = "synced";
        public static final String TS = "ts";
        public static final String COLUMN_NAME_NULLABLE = "null";
    }
	
}
