package com.peakbw.askarienrol.fragments;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.Toast;

import com.peakbw.askarienrol.R;
import com.peakbw.askarienrol.activities.EnrollmentActivity1;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
	private int year;
    private int viewID;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Use the current date as the default date in the picker

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        viewID = getArguments().getInt("view");

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year1, int month, int day) {

        String mnth = String.valueOf(month+1);
        String dy = String.valueOf(day);

        if(mnth.length()==1){
            mnth = "0"+mnth;
        }

        if(dy.length()==1){
            dy = "0"+dy;
        }

        if(viewID == EnrollmentActivity1.appointmentView){
            EnrollmentActivity1.appointmentField.setText(year1+"-"+mnth+"-"+dy);
        }
        else{
            int years  = year-year1;
            Log.d("DatePicker Fragment","Year = "+years+"\nCurrent Year : "+year+"\n Year of Birth : "+year1);
            if(years>17){
                EnrollmentActivity1.dateField.setText(year1+"-"+mnth+"-"+dy);
            }
            else{
                EnrollmentActivity1.dateField.setError("Staff is under aged");
                EnrollmentActivity1.dateField.requestFocus();
                //Toast.makeText(getActivity(), "invalid Date Of Birth!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
