package com.peakbw.askarienrol.net;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import com.peakbw.askarienrol.activities.MainActivity;
import com.peakbw.askarienrol.db.ReaderContract;
import com.peakbw.askarienrol.db.ReaderContract.Auth;
import com.peakbw.askarienrol.db.ReaderContract.Employee;
import com.peakbw.askarienrol.db.ReaderContract.Employer;
import com.peakbw.askarienrol.db.ReaderContract.Signature;
import com.peakbw.askarienrol.db.ReaderDBHelper;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.util.Xml;

public class XMLParser {
	private static final String DEBUG_TAG = "XMLParser";
    private static final String ns = null;
    private static String date;
    public static String signature = "",status,siteId,confirmationNum,trxnNum,siteName,method,version;
	private static ContentValues values = new ContentValues();


	
	public static String parseXML(InputStream is,Context context){//throws XmlPullParserException, IOException
        
        status = null;confirmationNum=null;trxnNum=null;method=null;
        
		try{

			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			byte[] buffer = new byte[1024];
			int len;
			while ((len = is.read(buffer)) > -1 ) {
				baos.write(buffer, 0, len);
			}
			baos.flush();
			InputStream is1 = new ByteArrayInputStream(baos.toByteArray());

			int ch ;
	        StringBuilder sb = new StringBuilder();
	        while((ch = is1.read())!=-1){
	            sb.append((char)ch);
	        }
	        Log.d(DEBUG_TAG, "Reply XML= "+sb.toString());


        	XmlPullParser parser = Xml.newPullParser();
        	parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        	parser.setInput(new ByteArrayInputStream(baos.toByteArray()), null);
        	parser.nextTag();
        	parser.require(XmlPullParser.START_TAG, ns, "pbtReply");
        	
        	String employeeId = "",action = "",
        			employerName = "",employerId = "",
        			employeeName = "",bankName = "",bankId = "";
        	
        	while (parser.next() != XmlPullParser.END_TAG) {
        		if (parser.getEventType() != XmlPullParser.START_TAG) {
        			continue;
        		}
        		String name = parser.getName();
        		Log.d(DEBUG_TAG, "TagName: "+name);
        		
        		if(name.equals("date")){
        			parser.require(XmlPullParser.START_TAG, ns, "date");
        			if (parser.next() == XmlPullParser.TEXT) {
        				date = (parser.getText()).substring(0,10);
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, "date");
        			Log.d(DEBUG_TAG, "Date: "+date);
        		}
        		else if(name.equals("signature")){
        			parser.require(XmlPullParser.START_TAG, ns, "signature");
        			if (parser.next() == XmlPullParser.TEXT) {
        				signature = parser.getText();
        				//update signature
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, "signature");
        			Log.d(DEBUG_TAG, "Signature: "+signature);
        		}
        		else if(name.equalsIgnoreCase("status")){
        			parser.require(XmlPullParser.START_TAG, ns, name);
        			if (parser.next() == XmlPullParser.TEXT) {
        				status = parser.getText();
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, name);
        			Log.d(DEBUG_TAG, "Status: "+status);
        		}
        		else if(name.equals("confNumber")){
        			parser.require(XmlPullParser.START_TAG, ns, "confNumber");
        			if (parser.next() == XmlPullParser.TEXT) {
        				confirmationNum = parser.getText();
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, "confNumber");
        			Log.d(DEBUG_TAG, "ConfNum: "+confirmationNum);
        		}
        		else if(name.equals("refNumber")){
        			parser.require(XmlPullParser.START_TAG, ns, "refNumber");
        			if (parser.next() == XmlPullParser.TEXT) {
        				trxnNum = parser.getText();
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, "refNumber");
        			Log.d(DEBUG_TAG, "refNumber: "+trxnNum);
        		}
        		else if(name.equals("method")){
        			parser.require(XmlPullParser.START_TAG, ns, "method");
        			if (parser.next() == XmlPullParser.TEXT) {
        				method = parser.getText();
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, "method");
        			Log.d(DEBUG_TAG, "Method: "+method);
        		}
        		/*else if(name.equals("upgradeURL")){
        			parser.require(XmlPullParser.START_TAG, ns, "upgradeURL");
        			if (parser.next() == XmlPullParser.TEXT) {
        				upgradeURL = parser.getText();
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, "upgradeURL");
        			Log.d(DEBUG_TAG, "upgradeURL: "+upgradeURL);
        		}*/
        		else if(name.equals("version")){
        			parser.require(XmlPullParser.START_TAG, ns, "version");
        			if (parser.next() == XmlPullParser.TEXT) {
        				version = parser.getText();
        		        parser.nextTag();
        		    }
        			parser.require(XmlPullParser.END_TAG, ns, "version");
        			Log.d(DEBUG_TAG, "version: "+version);
        		}
        		else if (name.equals("update")) {
        			parser.require(XmlPullParser.START_TAG, ns, "update");
        			while (parser.next() != XmlPullParser.END_TAG) {
        				if (parser.getEventType() != XmlPullParser.START_TAG) {
        	                continue;
        	            }
        				String name1 = parser.getName();
        				if (name1.equals("employers")) {
        					parser.require(XmlPullParser.START_TAG, ns, "employers");
        					while(parser.next()!= XmlPullParser.END_TAG){
        						if (parser.getEventType() != XmlPullParser.START_TAG) {
                	                continue;
                	            }
        						String name2 = parser.getName();
        						if (name2.equals("employer")) {
        							parser.require(XmlPullParser.START_TAG, ns, "employer");
        		        			while(parser.next()!= XmlPullParser.END_TAG){
        		        				if (parser.getEventType() != XmlPullParser.START_TAG) {
                        	                continue;
                        	            }
                						String name3 = parser.getName();
										switch (name3) {
											case "name":
												parser.require(XmlPullParser.START_TAG, ns, "name");
												if (parser.next() == XmlPullParser.TEXT) {
													employerName = parser.getText();
													parser.nextTag();
												}
												parser.require(XmlPullParser.END_TAG, ns, "name");
												Log.d(DEBUG_TAG, "Employer: " + employerName);
												break;
											case "id":
												parser.require(XmlPullParser.START_TAG, ns, "id");
												if (parser.next() == XmlPullParser.TEXT) {
													employerId = parser.getText();
													parser.nextTag();
												}
												parser.require(XmlPullParser.END_TAG, ns, "id");
												Log.d(DEBUG_TAG, "EmployerId: " + employerId);
												break;
											case "action":
												parser.require(XmlPullParser.START_TAG, ns, "action");
												if (parser.next() == XmlPullParser.TEXT) {
													action = parser.getText();
													parser.nextTag();
												}
												parser.require(XmlPullParser.END_TAG, ns, "action");
												Log.d(DEBUG_TAG, "Action: " + action);
												break;
											default:
												skip(parser);
												break;
										}
        		        			}
        		        			//if("INSERT".equalsIgnoreCase(action)){

									ReaderDBHelper dbHelper = new ReaderDBHelper(context);
									SQLiteDatabase db = dbHelper.getReadableDatabase();

        		        				// Create a new map of values, where column names are the keys
        		        		    	values.put(Employer.EMPLOYER_ID, employerId.trim());
        		        		    	values.put(Employer.EMPLOYER_NAME, employerName.trim());
        		        		    	
        		        		    	// Insert the new row, returning the primary key value of the new row
        		        		    	long newRowId = db.insertWithOnConflict(Employer.TABLE_NAME,Employer.COLUMN_NAME_NULLABLE,values,SQLiteDatabase.CONFLICT_IGNORE);
        		        		    	Log.d(DEBUG_TAG, "New Employer Row Id:"+ newRowId+" "+employerId);
        		        		    	Log.d(DEBUG_TAG, "New Employer: "+ employerName);
        		        		    	values.clear();
                                    /*}
                                    else if("DELETE".equalsIgnoreCase(action)){
                                    	
                                    }*/
									db.close();
									dbHelper.close();
        						}
        						else{
        							skip(parser);
        						}
        					}
        				}
        				else if (name1.equals("employees")) {
        					parser.require(XmlPullParser.START_TAG, ns, "employees");
        					while(parser.next()!= XmlPullParser.END_TAG){
        						if (parser.getEventType() != XmlPullParser.START_TAG) {
                	                continue;
                	            }
        						String name4 = parser.getName();
        						if (name4.equals("employee")) {
        							parser.require(XmlPullParser.START_TAG, ns, "employee");
        		        			while(parser.next()!= XmlPullParser.END_TAG){
        		        				if (parser.getEventType() != XmlPullParser.START_TAG) {
                        	                continue;
                        	            }
                						String name5 = parser.getName();
										switch (name5) {
											case "name":
												parser.require(XmlPullParser.START_TAG, ns, "name");
												if (parser.next() == XmlPullParser.TEXT) {
													employeeName = parser.getText();
													parser.nextTag();
												}
												parser.require(XmlPullParser.END_TAG, ns, "name");
												//Log.d(DEBUG_TAG, "EmployeeName: " + employeeName);
												break;
											case "id":
												parser.require(XmlPullParser.START_TAG, ns, "id");
												if (parser.next() == XmlPullParser.TEXT) {
													employeeId = parser.getText();
													parser.nextTag();
												}
												parser.require(XmlPullParser.END_TAG, ns, "id");
												//Log.d(DEBUG_TAG, "EmployeeID: " + employeeId);
												break;
											case "employerID":
												parser.require(XmlPullParser.START_TAG, ns, "employerID");
												if (parser.next() == XmlPullParser.TEXT) {
													employerId = parser.getText();
													parser.nextTag();
												}
												parser.require(XmlPullParser.END_TAG, ns, "employerID");
												//Log.d(DEBUG_TAG, "employerID: " + employerId);
												break;
											case "action":
												parser.require(XmlPullParser.START_TAG, ns, "action");
												if (parser.next() == XmlPullParser.TEXT) {
													action = parser.getText();
													parser.nextTag();
												}
												parser.require(XmlPullParser.END_TAG, ns, "action");
												//Log.d(DEBUG_TAG, action);
												break;
											default:
												skip(parser);
												break;
										}
        		        			}
        		        			//if("INSERT".equalsIgnoreCase(action)){

									ReaderDBHelper dbHelper = new ReaderDBHelper(context);
									SQLiteDatabase db = dbHelper.getReadableDatabase();

        		        				// Create a new map of values, where column names are the keys
        		        		    	values.put(Employee.EMPLOYEE_ID, employeeId.trim());
        		        		    	values.put(Employee.EMPLOYEE_NAME, employeeName.trim());
        		        		    	values.put(Employee.EMPLOYER_ID, employerId.trim());
        		        		    	
        		        		    	// Insert the new row, returning the primary key value of the new row
        		        		    	long newRowId = db.insertWithOnConflict(Employee.TABLE_NAME,Employee.COLUMN_NAME_NULLABLE,values,SQLiteDatabase.CONFLICT_IGNORE);
        		        		    	Log.d(DEBUG_TAG, "New Employee Row Id:"+ newRowId+" "+employeeId);
        		        		    	//Log.d(DEBUG_TAG, "New Employee: "+ employeeName);
                                        //Log.d(DEBUG_TAG, "New EmployerID: "+ employerId);
        		        		    	values.clear();
                                    /*}
                                    else if("DELETE".equalsIgnoreCase(action)){
                                    	
                                    }*/

									db.close();
									dbHelper.close();
        						}
        						else{
        							skip(parser);
        						}
        					}
        				}
						else if (name1.equals("sites")) {
							parser.require(XmlPullParser.START_TAG, ns, "sites");
							while(parser.next()!= XmlPullParser.END_TAG){
								if (parser.getEventType() != XmlPullParser.START_TAG) {
									continue;
								}
								String name2 = parser.getName();
								if (name2.equals("site")) {
									parser.require(XmlPullParser.START_TAG, ns, "site");
									while(parser.next()!= XmlPullParser.END_TAG){
										if (parser.getEventType() != XmlPullParser.START_TAG) {
											continue;
										}
										String name3 = parser.getName();
										switch (name3) {
											case "siteName":
												parser.require(XmlPullParser.START_TAG, ns, "siteName");
												if (parser.next() == XmlPullParser.TEXT) {
													siteName = parser.getText();
													parser.nextTag();
												}
												parser.require(XmlPullParser.END_TAG, ns, "siteName");
												Log.d(DEBUG_TAG, "Site Name: " + siteName);
												break;
											case "siteID":
												parser.require(XmlPullParser.START_TAG, ns, "siteID");
												if (parser.next() == XmlPullParser.TEXT) {
													siteId = parser.getText();
													parser.nextTag();
												}
												parser.require(XmlPullParser.END_TAG, ns, "siteID");
												Log.d(DEBUG_TAG, "Site Id: " + siteId);
												break;
											case "action":
												parser.require(XmlPullParser.START_TAG, ns, "action");
												if (parser.next() == XmlPullParser.TEXT) {
													action = parser.getText();
													parser.nextTag();
												}
												parser.require(XmlPullParser.END_TAG, ns, "action");
												Log.d(DEBUG_TAG, "Action: " + action);
												break;
											default:
												skip(parser);
												break;
										}
									}
									//if("INSERT".equalsIgnoreCase(action)){

									ReaderDBHelper dbHelper = new ReaderDBHelper(context);
									SQLiteDatabase db = dbHelper.getReadableDatabase();

									// Create a new map of values, where column names are the keys
									values.put(ReaderContract.Sites.SITE_ID, siteId.trim());
									values.put(ReaderContract.Sites.SITE_NAME, siteName.trim());

									// Insert the new row, returning the primary key value of the new row
									long newRowId = db.insertWithOnConflict(ReaderContract.Sites.TABLE_NAME,ReaderContract.Sites.COLUMN_NAME_NULLABLE,values,SQLiteDatabase.CONFLICT_IGNORE);
									Log.d(DEBUG_TAG, "New Site Row Id:"+ newRowId+" "+siteId);
									Log.d(DEBUG_TAG, "New Site: "+ siteName);
									values.clear();
                                    /*}
                                    else if("DELETE".equalsIgnoreCase(action)){

                                    }*/

									db.close();
									dbHelper.close();
								}
								else{
									skip(parser);
								}
							}
						}
						else if (name1.equals("banks")) {
							parser.require(XmlPullParser.START_TAG, ns, "banks");
							while(parser.next()!= XmlPullParser.END_TAG){
								if (parser.getEventType() != XmlPullParser.START_TAG) {
									continue;
								}
								String name2 = parser.getName();
								if (name2.equals("bank")) {
									parser.require(XmlPullParser.START_TAG, ns, "bank");
									while(parser.next()!= XmlPullParser.END_TAG){
										if (parser.getEventType() != XmlPullParser.START_TAG) {
											continue;
										}
										String name3 = parser.getName();
										switch (name3) {
											case "bankName":
												parser.require(XmlPullParser.START_TAG, ns, "bankName");
												if (parser.next() == XmlPullParser.TEXT) {
													bankName = parser.getText();
													parser.nextTag();
												}
												parser.require(XmlPullParser.END_TAG, ns, "bankName");
												Log.d(DEBUG_TAG, "Bank Name: " + bankName);
												break;
											case "bankID":
												parser.require(XmlPullParser.START_TAG, ns, "bankID");
												if (parser.next() == XmlPullParser.TEXT) {
													bankId = parser.getText();
													parser.nextTag();
												}
												parser.require(XmlPullParser.END_TAG, ns, "bankID");
												Log.d(DEBUG_TAG, "Bank Id: " + bankId);
												break;
											case "action":
												parser.require(XmlPullParser.START_TAG, ns, "action");
												if (parser.next() == XmlPullParser.TEXT) {
													action = parser.getText();
													parser.nextTag();
												}
												parser.require(XmlPullParser.END_TAG, ns, "action");
												Log.d(DEBUG_TAG, "Action: " + action);
												break;
											default:
												skip(parser);
												break;
										}
									}
									//if("INSERT".equalsIgnoreCase(action)){

									ReaderDBHelper dbHelper = new ReaderDBHelper(context);
									SQLiteDatabase db = dbHelper.getReadableDatabase();

									// Create a new map of values, where column names are the keys
									values.put(ReaderContract.Banks.BANK_ID, bankId.trim());
									values.put(ReaderContract.Banks.BANK_NAME, bankName.trim());

									// Insert the new row, returning the primary key value of the new row
									long newRowId = db.insertWithOnConflict(ReaderContract.Banks.TABLE_NAME,ReaderContract.Banks.COLUMN_NAME_NULLABLE,values,SQLiteDatabase.CONFLICT_IGNORE);
									Log.d(DEBUG_TAG, "New Bank Row Id:"+ newRowId+" "+bankId);
									Log.d(DEBUG_TAG, "New Bank: "+ bankName);
									values.clear();
                                    /*}
                                    else if("DELETE".equalsIgnoreCase(action)){

                                    }*/

									db.close();
									dbHelper.close();
								}
								else{
									skip(parser);
								}
							}
						}
        				else {
                			skip(parser);
                		}
        			}
        		}
        		else {
        			skip(parser);
        		}
        	}
        	
        	if(status.trim().equalsIgnoreCase("SUCCESSFUL")){
				addUser(context);
				updateSignature(context);
			}
        }
        catch(IOException ex){
        	ex.printStackTrace();
			Log.d(DEBUG_TAG,ex.getMessage());
        }
        catch(XmlPullParserException ex){
        	ex.printStackTrace();
        }
        return status.trim();
	}
	
	private static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
            case XmlPullParser.END_TAG:
                    depth--;
                    break;
            case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
	
	private static void addUser(Context context){
		String pin = MainActivity.password;
		String username = MainActivity.username;

		ReaderDBHelper dbHelper = new ReaderDBHelper(context);
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		values.clear();
		values.put(Auth.USER_PIN, pin);
    	values.put(Auth.USER_NAME, username);
    	
    	// Insert the new row, returning the primary key value of the new row
    	long newRowId = db.insertWithOnConflict(Auth.TABLE_NAME, Auth.COLUMN_NAME_NULLABLE, values, SQLiteDatabase.CONFLICT_IGNORE);
    	Log.d(DEBUG_TAG, "New Auth Row Id:"+ newRowId+" "+pin);
    	Log.d(DEBUG_TAG, "New User: "+ "user"+pin);
    	values.clear();

		db.close();
		dbHelper.close();
		
	}
	
	private static void updateSignature(Context context){
		ReaderDBHelper dbHelper = new ReaderDBHelper(context);
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		values.put(Signature.SIGNATURE, signature);
		values.put(Signature.DATE, date);
		
		int count = db.update(
		    Signature.TABLE_NAME,
		    values,
		    null,
		    null);
		
		Log.d(DEBUG_TAG, "Update= : "+count);
		values.clear();

		db.close();
		dbHelper.close();
	}
}
