package com.peakbw.askarienrol.services;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.peakbw.askarienrol.R;
import com.peakbw.askarienrol.activities.ConfirmationActivity;
import com.peakbw.askarienrol.activities.EmployerActivity;
import com.peakbw.askarienrol.activities.EnrollmentActivity1;
import com.peakbw.askarienrol.activities.EnrollmentActivity2;
import com.peakbw.askarienrol.db.ReaderContract;
import com.peakbw.askarienrol.db.ReaderDBHelper;
import com.peakbw.askarienrol.net.FTPClientFunctions;
import com.peakbw.askarienrol.net.ManageServerConnections;
import com.peakbw.askarienrol.net.XMLParser;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class FTPService extends Service {
    private static final String DEBUG_TAG ="FTPService";
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTNL_STORE = 100;
    private int count = 0,countf = 0;
    private String srcPath;
    private FTPClientFunctions ftp;
    private  ManageServerConnections mdc;
    private int build_version = Build.VERSION.SDK_INT;

    public FTPService() {

        ftp = new FTPClientFunctions();
        mdc = new ManageServerConnections();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        intent = intent;
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(DEBUG_TAG, "Service created");
        new SyncTask().execute(getString(R.string.ftpServerHost),getString(R.string.ftpServerUsername),getString(R.string.ftpServerPassword),getString(R.string.ftpServerPort));

        new OffLineTask().execute(getString(R.string.connUrl));
    }

    public class SyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            Log.d(DEBUG_TAG, "Count = "+count);
            if(count == 6){
                count = 0;
                return  makeConnection(url[0],url[1],url[2],Integer.parseInt(url[3]));
            }else{
                count++;
                return  "Count is "+count;
            }
        }
        @Override
        protected void onPostExecute(String result){
            Log.d(DEBUG_TAG, "Status = " + result);
            if(result!=null && result.equalsIgnoreCase("true")){
                Log.d(DEBUG_TAG, "SYNC Success : " + result);
            }
            delay(true);
        }
    }

    public class OffLineTask extends AsyncTask<String, Void, String> {
        private String status;
        private InputStream stream = null;
        private String stateId;

        @Override
        protected String doInBackground(String... url) {
            if(countf == 2) {
                countf = 0;
                try {
                    String request = createEnrollmentXML();
                    if (request.length() == 0) {
                        return null;
                    }
                    mdc.setUpdateVariable(request);
                    stream = mdc.downloadUrl(url[0]);
                    try {
                        status = XMLParser.parseXML(stream, getBaseContext());
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                } finally {
                    if (stream != null) {
                        try {
                            stream.close();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
                return status;
            }else{
                countf++;
                return  null;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if(result!=null){
                Log.d(DEBUG_TAG, "BG Results = "+result);
                //if(result.contains("SUCCESSFUL")&&XMLParser.method.equals("askari_enrollment")){
                Log.d(DEBUG_TAG, "State ID = "+stateId+" "+EnrollmentActivity2.STATE_COMPLETE);
                    ConfirmationActivity.updateStateTable(EnrollmentActivity2.STATE_COMPLETE,stateId,getBaseContext());

                    /*if(EmployerActivity.newStaff){

                        ConfirmationActivity.insertEmployee(employee,employeeId,employerId,stateId,getBaseContext());
                    }*/
                //}
            }
            delay(false);
        }

        private String createEnrollmentXML(){
            StringBuilder xmlStr = new StringBuilder();
            try{
                xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
                xmlStr.append("<pbtRequest>");
                xmlStr.append("<method>").append("askari_enrollment").append("</method>");

                ReaderDBHelper dbHelper = new ReaderDBHelper(getBaseContext());

                Cursor cursor = dbHelper.getFailedRecords();
                int count = cursor.getCount();
                if(count < 1){
                    cursor.close();
                    dbHelper.close();
                    return "";
                }
                cursor.moveToFirst();

                stateId = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State._ID));
                Log.d(DEBUG_TAG, "My State ID = "+stateId);

                String employer = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Employer.EMPLOYER_NAME));
                String employerId = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.EMPLOYER_ID));
                String employee = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Employee.EMPLOYEE_NAME));
                String employeeId = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.EMPLOYEE_ID));
                String siteId = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.SITE_ID));
                String bankId = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.BANK_ID));

                String nssf = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.NSSF_NUMBER));
                String account = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.ACCOUNT_NUMBER));
                String phone = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.PHONE_NUMBER));
                String gender = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.GENDER));
                String dob = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.DOB));
                String id = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.ID_FILE));
                String photo = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.PHOTO_FILE));
                String appointmentDate = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.APPOINTMENT_DATE));
                String staffPin = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.State.STAFF_PIN));

                cursor = dbHelper.getUserCredentials();
                cursor.moveToFirst();

                String password = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Auth.USER_PIN));
                String username = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Auth.USER_NAME));

                xmlStr.append("<password>").append(password).append("</password>");
                xmlStr.append("<username>").append(username).append("</username>");

                cursor = dbHelper.getSignature();
                cursor.moveToFirst();
                String signature = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Signature.SIGNATURE));
                cursor.close();
                dbHelper.close();

                xmlStr.append("<signature>").append(signature).append("</signature>");
                xmlStr.append("<trxnNumber>").append(ManageServerConnections.getTrxnNumber()).append("</trxnNumber>");
                xmlStr.append("<employerName>").append(employer).append("</employerName>");
                xmlStr.append("<employerID>").append(employerId.trim()).append("</employerID>");
                xmlStr.append("<employeeName>").append(employee).append("</employeeName>");
                xmlStr.append("<employeeID>").append(employeeId.trim()).append("</employeeID>");
                xmlStr.append("<siteId>").append(siteId.trim()).append("</siteId>");
                xmlStr.append("<bankId>").append(bankId.trim()).append("</bankId>");
                xmlStr.append("<nssf>").append(nssf).append("</nssf>");
                xmlStr.append("<accNum>").append(account).append("</accNum>");
                String phoneNumber = phone.replaceFirst("0","256");
                xmlStr.append("<phoneNumber>").append(phoneNumber).append("</phoneNumber>");
                xmlStr.append("<gender>").append(gender).append("</gender>");
                xmlStr.append("<dateOfBirth>").append(dob).append("</dateOfBirth>");
                xmlStr.append("<idScan>").append(id).append("</idScan>");
                xmlStr.append("<photo>").append(photo).append("</photo>");
                xmlStr.append("<appointmentDate>").append(appointmentDate).append("</appointmentDate>");
                xmlStr.append("<employeePin>").append(staffPin.trim()).append("</employeePin>");
                xmlStr.append("</pbtRequest>");

                System.out.println(xmlStr.toString());
            }catch (Exception ex){
                ex.printStackTrace();
            }
            return xmlStr.toString();
        }
    }

    private void delay(final boolean ftpProcess){
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                if(ftpProcess){
                    new SyncTask().execute(getString(R.string.ftpServerHost),getString(R.string.ftpServerUsername),getString(R.string.ftpServerPassword),getString(R.string.ftpServerPort));
                }else{
                    new OffLineTask().execute(getString(R.string.connUrl));
                }
            }
        }, 60000);
    }

    @TargetApi(23)
    private String makeConnection(String host,String username,String password,int port){
        String status = null;
        boolean connected,uploaded,disconnected;

        //get pics to be synced from db
        ReaderDBHelper dbHelper = new ReaderDBHelper(this);
        Cursor cursor = dbHelper.getPics();

        try{
            if(cursor.getCount()>0){
                connected = ftp.ftpConnect(host,username,password,port);
                Log.d(DEBUG_TAG,"Connected To FTP Server = "+connected);
                if(connected){
                    String workingDirectory = ftp.ftpGetCurrentWorkingDirectory();
                    Log.d(DEBUG_TAG,"FTP Server Working Directory = "+workingDirectory);

                    while(cursor.moveToNext()){
                        String project = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Pics.PROJECT));
                        String desDirectory = workingDirectory/*+getString(R.string.ftpRemoteDirectory)*/+project;
                        Log.d(DEBUG_TAG,"FTP Destination Directory = "+desDirectory);

                        srcPath = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Pics.NAME));
                        assert srcPath != null;
                        Log.d(DEBUG_TAG,"Source Path = "+srcPath);
                        if(srcPath!=null || !srcPath.equals("")){
                            String picName = extractName(srcPath);
                            Log.d(DEBUG_TAG,"Pic Name = "+picName);

                            //create temporary file
                            //String tempFile = srcPath.substring(0,srcPath.length()-3)+"temp";
                            String tempFile = srcPath;
                            Log.d(DEBUG_TAG,"Temporary File = "+tempFile);

                            File original = new File(srcPath);
                            File temp = new File(tempFile);

                            boolean ori_to_temp = false;
                            if(build_version > 22){

                                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission((Activity)this.getBaseContext(),Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions((Activity)this.getBaseContext(),new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},MY_PERMISSIONS_REQUEST_READ_EXTNL_STORE);
                                    ActivityCompat.requestPermissions((Activity)this.getBaseContext(),new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},MY_PERMISSIONS_REQUEST_READ_EXTNL_STORE);
                                }else{
                                    ori_to_temp = original.renameTo(temp);
                                }
                            }else{
                                ori_to_temp = original.renameTo(temp);
                            }
                            Log.d(DEBUG_TAG,"Orignal to Temp = "+ori_to_temp);

                            String tempDesPath = desDirectory+"/"+extractName(tempFile);
                            Log.d(DEBUG_TAG,"Temporary Destination Path = "+tempDesPath);

                            String finalDesPath = desDirectory+"/"+picName;
                            Log.d(DEBUG_TAG,"Final Destination Path = "+finalDesPath);

                            uploaded = ftp.ftpUpload(tempFile,tempDesPath,"/docs/demo/",this);
                            Log.d(DEBUG_TAG,"File Uploaded = "+uploaded);

                            boolean renamed = false;

                            if(uploaded){

                                renamed = ftp.ftpRenameFile(tempDesPath,finalDesPath);
                                if(renamed){
                                    updatePicksTable(srcPath,this);
                                }
                            }

                            Log.d(DEBUG_TAG,"Renamed to final = "+renamed);

                            boolean tem_to_orig = temp.renameTo(original);

                            Log.d(DEBUG_TAG,"Temp to Orignal = "+tem_to_orig);

                            status = String.valueOf(uploaded);

                        }
                    }
                }
                disconnected = ftp.ftpDisconnect();
                Log.d(DEBUG_TAG,"Loged out = "+disconnected);
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }

        cursor.close();
        dbHelper.close();


        return  status;
    }

    public static String extractName(String path){
        String [] files = path.split("/");
        return  files[files.length-1];
    }

    public static void updatePicksTable(String srcPath, Context context){
        //updae pics table
        ReaderDBHelper dbHelper = new ReaderDBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // New value for one column
        ContentValues values = new ContentValues();
        values.put(ReaderContract.Pics.SYNCED, true);

        // Which row to update, based on the ID
        String selection = ReaderContract.Pics.NAME + " = ?";
        String[] selectionArgs = {srcPath};

        int count = db.update(ReaderContract.Pics.TABLE_NAME,values,selection,selectionArgs);

        db.close();
        dbHelper.close();

        values.clear();
        Log.d(DEBUG_TAG, "Affected Row ID = "+count);
    }
}
