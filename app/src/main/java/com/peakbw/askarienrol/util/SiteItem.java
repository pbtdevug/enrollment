package com.peakbw.askarienrol.util;

public class SiteItem {
    private String siteId;
    private String siteName;

    public SiteItem(){}

    public String getSiteId(){
        return siteId;
    }

    public String getSiteName(){
        return siteName;
    }

    public void setSiteId(String id){
        this.siteId = id;
    }

    public void setSiteName(String name){
        this.siteName = name;
    }
}
