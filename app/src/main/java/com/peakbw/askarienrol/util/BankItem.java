package com.peakbw.askarienrol.util;

public class BankItem {
    private String bankId;
    private String bankName;

    public BankItem(){}

    public String getBankId(){
        return bankId;
    }

    public String getBankName(){
        return bankName;
    }

    public void setBankId(String id){
        this.bankId = id;
    }

    public void setBankName(String name){
        this.bankName = name;
    }
}
