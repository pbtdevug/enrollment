package com.peakbw.askarienrol.util;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import com.peakbw.askarienrol.R;
import com.peakbw.askarienrol.db.ReaderContract;
import com.peakbw.askarienrol.db.ReaderDBHelper;
import com.peakbw.askarienrol.net.FTPClientFunctions;
import com.peakbw.askarienrol.services.FTPService;

import java.io.File;

public class SyncTask extends AsyncTask<String, Void, String> {
    private static final String DEBUG_TAG ="SyncTask";
    private FTPClientFunctions ftp;
    private Context context;

    public SyncTask(Context callingActivity){
        ftp = new FTPClientFunctions();
        context = callingActivity;
    }

    @Override
    protected String doInBackground(String... url) {
        return  makeConnection(url[0],url[1],url[2],Integer.parseInt(url[3]));
    }
    @Override
    protected void onPostExecute(String result){
        Log.d(DEBUG_TAG, "Status = " + result);
        if(result!=null && result.equalsIgnoreCase("true")){
            Log.d(DEBUG_TAG, "SYNC Success : " + result);
        }
    }

    private String makeConnection(String host,String username,String password,int port){
        String status = null;
        boolean connected,uploaded,disconnected;

        //get pics to be synced from db
        ReaderDBHelper dbHelper = new ReaderDBHelper(context);
        Cursor cursor = dbHelper.getPics();

        try{
            if(cursor.getCount()>0){
                connected = ftp.ftpConnect(host,username,password,port);
                Log.d(DEBUG_TAG,"Connected To FTP Server = "+connected);
                if(connected){
                    String workingDirectory = ftp.ftpGetCurrentWorkingDirectory();
                    Log.d(DEBUG_TAG,"FTP Server Working Directory = "+workingDirectory);

                    while(cursor.moveToNext()){
                        String project = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Pics.PROJECT));
                        String desDirectory = workingDirectory/*+context.getString(R.string.ftpRemoteDirectory)*/+project;
                        Log.d(DEBUG_TAG,"FTP Destination Directory = "+desDirectory);

                        String srcPath = cursor.getString(cursor.getColumnIndexOrThrow(ReaderContract.Pics.NAME));
                        assert srcPath != null;
                        Log.d(DEBUG_TAG,"Source Path = "+srcPath);
                        if(srcPath!=null || !srcPath.equals("")){
                            String picName = FTPService.extractName(srcPath);
                            Log.d(DEBUG_TAG,"Pic Name = "+picName);

                            //create temporary file
                            String tempFile = srcPath.substring(0,srcPath.length()-3)+"temp";
                            Log.d(DEBUG_TAG,"Temporary File = "+tempFile);

                            File original = new File(srcPath);
                            File temp = new File(tempFile);

                            boolean ori_to_temp = original.renameTo(temp);
                            Log.d(DEBUG_TAG,"Orignal to Temp = "+ori_to_temp);

                            String tempDesPath = desDirectory+"/"+ FTPService.extractName(tempFile);
                            Log.d(DEBUG_TAG,"Temporary Destination Path = "+tempDesPath);

                            String finalDesPath = desDirectory+"/"+picName;
                            Log.d(DEBUG_TAG,"Final Destination Path = "+finalDesPath);

                            uploaded = ftp.ftpUpload(tempFile,tempDesPath,"",context);
                            Log.d(DEBUG_TAG,"File Uploaded = "+uploaded);

                            boolean renamed = false;

                            if(uploaded){

                                renamed = ftp.ftpRenameFile(tempDesPath,finalDesPath);
                                if(renamed){
                                    FTPService.updatePicksTable(srcPath,context);
                                }
                            }

                            Log.d(DEBUG_TAG,"Renamed to final = "+renamed);

                            boolean tem_to_orig = temp.renameTo(original);

                            Log.d(DEBUG_TAG,"Temp to Orignal = "+tem_to_orig);

                            status = String.valueOf(uploaded);

                        }
                    }
                }
                disconnected = ftp.ftpDisconnect();
                Log.d(DEBUG_TAG,"Loged out = "+disconnected);
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }

        cursor.close();
        dbHelper.close();


        return  status;
    }
}
